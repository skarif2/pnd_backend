import moment from 'moment';
import User from '../models/user.model';
import FCMHelper from '../helpers/FCMHelper';
import SMSHelper from '../helpers/SMSHelper';

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function dateTest(req, res) {
  const currentDate = new Date();
  const newDate = moment(currentDate).format('YYYY-MM-DD');
  const oldDate1 = moment('2017-05-01 14:01:08.503').format('YYYY-MM-DD');
  const year = moment('2017-05-01 14:01:08.503').year();
  const oldDate2 = moment('2017-05-04 14:01:08.503').format('HH:MM');
  // const oldDate = moment(currentDate).format('ll');
  if (moment(newDate).isAfter(oldDate1)) {
    res.json({
      status: 'success',
      newDate,
      oldDate1,
      oldDate2,
      year
    });
  } else {
    res.json({
      status: 'error'
    });
  }
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function timeTest(req, res) {
  // const startTime = '570';
  // const endTime = '1040';
  // const currentDate = new Date(1493985600000);
  // const hour = moment(currentDate).hour();
  // const minute = moment(currentDate).minute();
  // const currentTime = (hour * 60) + minute;
  // res.json({
  //   startTime,
  //   endTime,
  //   hour,
  //   minute,
  //   currentTime
  // });
  // const currentDate = new Date();


  // const now = '04/09/2013 16:00:00';
  // const then = '04/09/2013 14:21:30';
  // const ms = moment(now, 'YYYY-MM-DD HH:mm:ss').diff(moment(then, 'YYYY-MM-DD HH:mm:ss'));
  // const duration = moment.duration(ms, 'milliseconds');
  // const minutes = Math.floor(duration.asMinutes());
  // const newTime = new Date(parseInt(req.body.currentTime, 10));
  const time = moment(1495388120135).format('YYYY-MM-DD HH:mm:ss');

  res.json({
    time
  });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function testPush(req, res) {
  FCMHelper.bulkPush([req.body.pushCode], 'This is a test push.');
  res.json({
    status: 'success',
    code: req.body.pushCode
  });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function testPushUser(req, res) {
  User.getByMobileNumber(req.body.mobileNumber)
    .then((user) => {
      FCMHelper.bulkPush([user.pushCode], req.body.message);
      res.json({
        status: 'success',
        name: user.name,
        pushCode: user.pushCode
      });
    })
    .catch((err) => {
      res.json({
        err
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function testSMS(req, res) {
  SMSHelper.send(req.body.mobileNumber, req.body.message)
    .then(() => {
      res.json({
        status: 'success',
        code: req.body.mobileNumber,
        message: req.body.message
      });
    })
    .catch((err) => {
      res.json({
        err
      });
    });
}

export default {
  dateTest,
  timeTest,
  testPush,
  testPushUser,
  testSMS
};
