import bcrypt from 'bcryptjs';
import moment from 'moment';
import httpStatus from 'http-status';
import strings from '../../config/strings';
import APIError from '../helpers/APIError';
import User from '../models/user.model';
import Driver from '../models/driver.model';
import Option from '../models/option.model';
import Request from '../models/request.model';
import Trip from '../models/trip.model';
import FinishedTrip from '../models/finishedTrip.model';
import UnfinishedTrip from '../models/unfinishedTrip.model';
import Pole from '../models/pole.model';
import Prepole from '../models/prepole.model';
import Warning from '../models/warning.model';
import IMGHelper from '../helpers/IMGHelper';
import RNDHelper from '../helpers/RNDHelper';
import MAPHelper from '../helpers/MAPHelper';
import FCMHelper from '../helpers/FCMHelper';
import SMSHelper from '../helpers/SMSHelper';

const salt = bcrypt.genSaltSync(10);

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res) {
  let bonus = 0;
  User.getToCheckExists(req.body.mobileNumber)
    .then(() => Option.find())
    .then((option) => {
      bonus = option[0].bonus.signUp.value;
      if (req.body.image && req.body.image !== '') {
        return IMGHelper.userImage(req.body.image);
      }
      return 'default.png';
    })
    .then((imageName) => {
      const verificationCode = RNDHelper.verificationCode();
      const user = new User({
        name: req.body.name,
        mobileNumber: req.body.mobileNumber,
        image: imageName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, salt),
        account: {
          balance: bonus
        },
        verification: {
          code: verificationCode
        }
      });
      SMSHelper.sendVerification(req.body.mobileNumber, verificationCode);
      return user.save();
    })
    .then(() => {
      const mobileNumber = req.body.mobileNumber.substr(req.body.mobileNumber.length - 2);
      res.json({
        status: 'success',
        message: `Signup successful. A verification code has been sent to: ***** ****${mobileNumber}`
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Verify user
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @property {string} req.body.code - The verification code of user.
 * @returns {User}
 */
function verify(req, res) {
  User.getToVerify(req.body.mobileNumber, req.body.code)
    .then((user) => {
      const saveUser = Object.create(user);
      saveUser.verification.verified = true;
      return saveUser.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Verification success.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Generate and send new password to user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function rePassword(req, res) {
  User.getByMobileNumber(req.body.mobileNumber)
    .then((user) => {
      const saveUser = Object.create(user);
      const newPassword = RNDHelper.password();
      saveUser.password = bcrypt.hashSync(newPassword, salt);
      SMSHelper.sendPassword(req.body.mobileNumber, newPassword);
      return saveUser.save();
    })
    .then(() => {
      const mobileNumber = req.body.mobileNumber.substr(req.body.mobileNumber.length - 2);
      res.json({
        status: 'success',
        message: `A text message with your new password has been sent to: ***** ****${mobileNumber}`
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Resend verification code to user's mobileNumber
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function reVerify(req, res) {
  User.getByMobileNumber(req.body.mobileNumber)
    .then((user) => {
      const saveUser = Object.create(user);

      // bellow line will be deleted once sms is solved.
      saveUser.verification.code = RNDHelper.verificationCode();

      const verificationCode = RNDHelper.verificationCode();
      saveUser.verification.code = verificationCode;
      SMSHelper.sendVerification(req.body.mobileNumber, verificationCode);
      return saveUser.save();
    })
    .then(() => {
      const mobileNumber = req.body.mobileNumber.substr(req.body.mobileNumber.length - 2);
      res.json({
        status: 'success',
        message: `A new verification code has been sent to: ***** ****${mobileNumber}`
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Trip history of user
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function history(req, res) {
  FinishedTrip.list(req.body.userId)
    .then((finishedTrips) => {
      const sortedTrips = finishedTrips.map((trip) => {
        const startDate = moment(trip.tripTiming.start).format('ll');
        const startTime = moment(trip.tripTiming.start).format('LT');
        return {
          startDate,
          startTime,
          tripId: trip._id,
          from: trip.route.from.text,
          to: trip.route.to.text,
          snapshot: trip.snapshot
        };
      });
      res.json({
        status: 'success',
        history: sortedTrips
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Trip history details of user
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function detailedHistory(req, res) {
  FinishedTrip.get(req.body.tripId)
    .then((finishedTrip) => {
      res.json({
        status: 'success',
        snapshot: finishedTrip.snapshot,
        startDate: moment(finishedTrip.tripTiming.start).format('ll'),
        startTime: moment(finishedTrip.tripTiming.start).format('LT'),
        driver: finishedTrip.driver,
        route: finishedTrip.route,
        billing: finishedTrip.billing
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Account balance of user
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function account(req, res) {
  User.get(req.body.userId)
    .then((user) => {
      res.json({
        status: 'success',
        balance: user.account.balance,
        spent: user.account.spent
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Account balance of user
 * @property {string} req.body.userId - The userId of user.
 * @property {string} req.body.oldPassword - Previous password of user.
 * @property {string} req.body.newPassword - New password of user.
 * @property {string} req.body.lat - Latitude of user.
 * @property {string} req.body.lng - Longitude of user.
 * @property {string} req.body.name - New name of user.
 * @property {string} req.body.email - New email of user.
 * @property {string} req.body.image - New image of user.
 * @property {string} req.body.pushCode - New pushCode of user.
 * @returns {User}
 */
function update(req, res) {
  User.get(req.body.userId)
    .then((user) => {
      if (user.device.deviceId !== req.body.deviceId) {
        return res.json({
          status: 'logout',
          message: 'You are logged in on another device. Please try re-login.'
        });
      }
      if (!(req.body.lat && req.body.lng) && !req.body.pushCode
        && !(req.body.oldPassword && req.body.newPassword)
        && !(req.body.name || req.body.email || req.body.image)) {
        throw new APIError('Please check your parameters.', httpStatus.NOT_FOUND);
      }
      const saveUser = Object.create(user);
      saveUser.location.lat = req.body.lat ? req.body.lat : saveUser.location.lat;
      saveUser.location.lng = req.body.lng ? req.body.lng : saveUser.location.lng;
      saveUser.pushCode = req.body.pushCode ? req.body.pushCode : saveUser.pushCode;
      saveUser.name = req.body.name ? req.body.name : saveUser.name;
      saveUser.email = req.body.email ? req.body.email : saveUser.email;
      if (req.body.oldPassword && req.body.newPassword) {
        if (bcrypt.compareSync(req.body.oldPassword, user.password)) {
          saveUser.password = bcrypt.hashSync(req.body.newPassword, salt);
        } else {
          throw new APIError('Current password didn\'t match.', httpStatus.NOT_FOUND);
        }
      }
      if (req.body.image && req.body.image !== '') {
        IMGHelper.userImage(req.body.image)
          .then((imageName) => {
            IMGHelper.deleteImage(saveUser.image);
            saveUser.image = imageName;
            return saveUser.save();
          })
          .catch((err) => {
            throw err;
          });
      }
      return saveUser.save();
    })
    .then((savedUser) => {
      if (savedUser._id) {
        res.json({
          status: 'success',
          message: 'Update successful.'
        });
      }
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Available drivers for trip.
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function availableDrivers(req, res) {
  let area = 3;
  Option.find()
    .then((options) => {
      if (req.body.vehicleType === 'bike') {
        area = options[0].area.bike;
      } else if (req.body.vehicleType === 'car') {
        area = options[0].area.car;
      }
      return Driver.getAvailable(req.body.vehicleType);
    })
    .then((drivers) => {
      const available = drivers.filter((toFilter) => {
        const distance = MAPHelper.getDistance(req.body.currentLat, req.body.currentLng,
          toFilter.location.lat, toFilter.location.lng);
        return (distance <= area);
      }).map(driver => ({
        name: driver.name,
        image: driver.image,
        lat: driver.location.lat,
        lng: driver.location.lng
      }));
      res.json({
        status: 'success',
        available
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Fare estimation for trip.
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function estimation(req, res) {
  let duration = 0;
  MAPHelper.getDuration(req.body.from, req.body.to)
    .then((response) => {
      duration = Math.ceil(response.rows[0].elements[0].duration.value / 60);
      return Option.find();
    })
    .then((options) => {
      let cost = 0;
      let baseFare = 0;
      let ttPerMin = 0;
      if (req.body.vehicleType === 'bike') {
        cost = (duration * options[0].billing.bike.ttPerMin) + options[0].billing.bike.baseFare;
        baseFare = options[0].billing.bike.baseFare;
        ttPerMin = options[0].billing.bike.ttPerMin;
      } else if (req.body.vehicleType === 'car') {
        cost = (duration * options[0].billing.car.ttPerMin) + options[0].billing.car.baseFare;
        baseFare = options[0].billing.car.baseFare;
        ttPerMin = options[0].billing.car.ttPerMin;
      }
      res.json({
        status: 'success',
        duration,
        cost,
        baseFare,
        ttPerMin
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Fare estimation for trip.
 * @property {string} req.body.userId - The userId of user.
 * @returns {User}
 */
function createRequest(req, res) {
  let area = 3;
  let maxCancel = 10;
  let pushCodes = [];
  let requestId = '';
  let userInfo = {};

  Trip.getToCheckExists(req.body.userId)
    .then(() => Request.getToCheckExists())
    .then(() => Option.find())
    .then((options) => {
      if (options[0].maintenance.isOn === true) {
        throw new APIError(`We are currently going through a maintenance. We will be available from ${options[0].maintenance.endDate}.`, httpStatus.NOT_FOUND);
      }
      const currentDate = new Date();
      const currentTime = (moment(currentDate).hour() * 60) + moment(currentDate).minute();
      if (options[0].serviceTime.open > currentTime || options[0].serviceTime.close < currentTime) {
        throw new APIError(`We are only available from ${options[0].serviceTime.open} to ${options[0].serviceTime.close}.`, httpStatus.NOT_FOUND);
      }
      maxCancel = options[0].maxCancel;
      if (req.body.vehicleType === 'bike') {
        area = options[0].area.bike;
      } else if (req.body.vehicleType === 'car') {
        area = options[0].area.car;
      }
      return User.get(req.body.userId);
    })
    .then((user) => {
      userInfo = user;
      const saveUser = Object.create(user);
      const currentDate = new Date();
      const newDate = moment(currentDate).format('YYYY-MM-DD');
      const oldDate = moment(user.cancelToday.date).format('YYYY-MM-DD');
      if (moment(newDate).isAfter(oldDate)) {
        saveUser.cancelToday.count = 0;
        saveUser.cancelToday.date = currentDate;
      } else if (moment(newDate).isSame(oldDate) && maxCancel < user.cancelToday.count) {
        throw new APIError('You have been temporarily blocked for cancelling too many trips. You will be unblocked tomorrow. Please call 01788993388 for help.', httpStatus.NOT_FOUND);
      }
      saveUser.location.lat = req.body.currentLat;
      saveUser.location.lng = req.body.currentLng;
      saveUser.save();
      return Driver.getAvailable(req.body.vehicleType);
    })
    .then((drivers) => {
      const available = drivers.filter((toFilter) => {
        const distance = MAPHelper.getDistance(req.body.fromLat, req.body.fromLng,
          toFilter.location.lat, toFilter.location.lng);
        return (distance <= area);
      }).map(driver => ({
        publicId: driver.publicId,
        pushCode: driver.pushCode
      }));
      if (available.length === 0) {
        const savePrepole = new Prepole({
          vehicleType: req.body.vehicleType,
          user: {
            userId: userInfo._id,
            mobileNumber: userInfo.mobileNumber,
            image: userInfo.image,
            name: userInfo.name
          },
          route: {
            from: {
              text: req.body.from,
              lat: req.body.fromLat,
              lng: req.body.fromLng
            },
            to: {
              text: req.body.to,
              lat: req.body.toLat,
              lng: req.body.toLng
            }
          }
        });
        savePrepole.save()
          .then(() => {})
          .catch(() => {});
        throw new APIError('All drivers are busy. Please try after sometime.', httpStatus.NOT_FOUND);
      }
      const saveRequest = new Request({
        drivers: available,
        vehicleType: req.body.vehicleType,
        userId: userInfo._id,
        route: {
          from: {
            text: req.body.from,
            lat: req.body.fromLat,
            lng: req.body.fromLng
          },
          to: {
            text: req.body.to,
            lat: req.body.toLat,
            lng: req.body.toLng
          }
        }
      });
      const savePole = new Pole({
        drivers: available,
        vehicleType: req.body.vehicleType,
        user: {
          userId: userInfo._id,
          mobileNumber: userInfo.mobileNumber,
          image: userInfo.image,
          name: userInfo.name
        },
        route: {
          from: {
            text: req.body.from,
            lat: req.body.fromLat,
            lng: req.body.fromLng
          },
          to: {
            text: req.body.to,
            lat: req.body.toLat,
            lng: req.body.toLng
          }
        }
      });
      savePole.save();
      return saveRequest.save();
    })
    .then((savedRequest) => {
      pushCodes = savedRequest.drivers.map(e => e.pushCode);
      const drivers = savedRequest.drivers.map(e => e.publicId);
      requestId = savedRequest._id;
      const warning = new Warning({
        drivers,
        requestId
      });
      return warning.save();
    })
    .then((savedWarning) => {
      const pushData = {
        userId: userInfo._id,
        name: userInfo.name,
        image: userInfo.image,
        rating: userInfo.rating.value.toFixed(1),
        from: req.body.from,
        to: req.body.to,
        warningId: savedWarning._id,
        requestId
      };
      FCMHelper.pushWithoutTag(pushCodes, 'Request', pushData);
      res.json({
        status: 'success',
        requestId,
        pushData
      });
      setTimeout(() => {
        Request.remove({ _id: requestId })
          .then((response) => {
            if (response.result.n !== 0) {
              if (userInfo.device.deviceOs === 'Android') {
                FCMHelper.pushWithTag([userInfo.pushCode], 'Busy', strings.fcmBodyBusy);
              } else {
                FCMHelper.pushWithoutTag([userInfo.pushCode], 'Busy', strings.fcmBodyBusy);
              }
            }
          });
        Warning.get(savedWarning._id)
          .then((warnings) => {
            warnings.drivers.forEach((publicId) => {
              Driver.getByPublicId(publicId)
                .then((driver) => {
                  const saveDriver = Object.create(driver);
                  const currentDate = new Date();
                  const newDate = moment(currentDate).format('YYYY-MM-DD');
                  const oldDate = moment(driver.warning.today.date).format('YYYY-MM-DD');
                  if (moment(newDate).isAfter(oldDate)) {
                    saveDriver.warning.today.date = currentDate;
                    saveDriver.warning.today.accept = 1;
                    saveDriver.warning.today.limit = 0;
                    saveDriver.warning.today.appKill = 0;
                  } else {
                    saveDriver.warning.today.accept += 1;
                  }
                  saveDriver.warning.total.accept += 1;
                  saveDriver.warning.total.limit += 1;
                  saveDriver.warning.total.appKill += 1;
                  saveDriver.save();
                });
            });
            Warning.remove({ _id: savedWarning._id })
              .then(() => {
                // console.log('warning removed');
              })
              .catch(() => {
                // console.log(err.message);
              });
          });
      }, 60 * 1000);
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Delete request
 * @property {ObjectId} req.body.requestId - The requestId to be deleted.
 * @returns {User}
 */
function deleteRequest(req, res) {
  Request.remove({ _id: req.body.requestId })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Trip successfully cancelled.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Delete request
 * @property {ObjectId} req.body.requestId - The requestId to be deleted.
 * @returns {User}
 */
function deleteTrip(req, res) {
  const currentDate = new Date();
  let tripInfo = {};
  let driverInfo = {};
  let userInfo = {};
  Trip.get(req.body.tripId)
    .then((trip) => {
      if (trip.tripState === 2) {
        throw new APIError('Can\'t proceed! Trip already started.', httpStatus.NOT_FOUND);
      }
      tripInfo = trip;
      return Trip.remove({ _id: trip._id });
    })
    .then(() => User.get(tripInfo.userId))
    .then((user) => {
      userInfo = user;
      const saveUser = Object.create(user);
      const newDate = moment(currentDate).format('YYYY-MM-DD');
      const oldDate = moment(user.cancelToday.date).format('YYYY-MM-DD');
      if (moment(newDate).isAfter(oldDate)) {
        saveUser.cancelToday.date = currentDate;
        saveUser.cancelToday.count = 1;
      } else {
        saveUser.cancelToday.count += 1;
      }
      return saveUser.save();
    })
    .then(() => Driver.get(tripInfo.driverId))
    .then((driver) => {
      driverInfo = driver;
      const saveDriver = Object.create(driver);
      saveDriver.status.isPendingBill = false;
      saveDriver.status.isOnTrip = false;
      saveDriver.status.isActive = false;
      return saveDriver.save();
    })
    .then(() => {
      const unfinishedTrip = new UnfinishedTrip({
        tripId: tripInfo._id,
        user: {
          userId: userInfo._id,
          mobileNumber: userInfo.mobileNumber,
          name: userInfo.name,
          image: userInfo.image
        },
        driver: {
          driverId: driverInfo.driverId,
          publicId: driverInfo.publicId,
          mobileNumber: driverInfo.mobileNumber,
          name: driverInfo.name,
          image: driverInfo.image
        },
        vehicleType: tripInfo.vehicleType,
        route: tripInfo.route,
        tripTiming: {
          accept: tripInfo.tripTiming.accept,
          arrive: tripInfo.tripTiming.arrive,
          start: tripInfo.tripTiming.start,
          end: tripInfo.tripTiming.end
        },
        tripState: tripInfo.tripState,
        cancelledBy: 'User'
      });
      return unfinishedTrip.save();
    })
    .then(() => {
      FCMHelper.pushWithTag([driverInfo.pushCode], 'Cancel', strings.userCancel);
      res.json({
        status: 'success',
        message: 'Trip cancelled successfully.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Rate driver
 * @property {ObjectId} req.body.finishTripId - The _id of finishedTrip.
 * @property {String} req.body.publicId - The publicId of driver.
 * @property {Number} req.body.rating - The rating value for driver.
 * @returns {User}
 */
function rate(req, res) {
  let finishedTripInfo = {};
  FinishedTrip.get(req.body.finishTripId)
    .then((finishedTrip) => {
      finishedTripInfo = finishedTrip;
      const saveFinishedTrip = Object.create(finishedTrip);
      saveFinishedTrip.rating.user = req.body.rating;
      return saveFinishedTrip.save();
    })
    .then(() => Driver.get(finishedTripInfo.driver.driverId))
    .then((driver) => {
      const saveDriver = Object.create(driver);
      const rating = ((driver.rating.value * driver.rating.count)
        + req.body.rating) / (driver.rating.count + 1);
      saveDriver.rating.count += 1;
      saveDriver.rating.value = rating;
      return saveDriver.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Thank you for using Pick N Drop.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Get state
 * @property {ObjectId} req.body.userId - The _id of user.
 * @property {deviceId} req.body.deviceId - The deviceId of user.
 * @returns {User}
 */
function state(req, res) {
  let tripInfo = {};
  User.get(req.body.userId)
    .then((user) => {
      if (user.device.deviceId !== req.body.deviceId) {
        res.json({
          status: 'Logout',
          message: 'You are currently logged in to another device. Please login to this device.'
        });
        throw new APIError('done', httpStatus.NOT_FOUND);
      }
      return Trip.findOne({ userId: req.body.userId });
    })
    .then((trip) => {
      tripInfo = trip;
      return Driver.get(trip.driverId);
    })
    .then((driver) => {
      const sendData = {
        status: 'Accept',
        tripId: tripInfo._id,
        route: tripInfo.route,
        driver: {
          publicId: driver.publicId,
          name: driver.name,
          image: driver.image,
          mobileNumber: driver.mobileNumber,
          rating: driver.rating.value.toFixed(1),
          lat: driver.location.lat,
          lng: driver.location.lng
        },
        vehicle: driver.vehicle
      };
      if (tripInfo.tripState === 0) {
        sendData.status = 'Accept';
      } else if (tripInfo.tripState === 1) {
        sendData.status = 'Arrive';
      } else if (tripInfo.tripState === 2) {
        sendData.status = 'Start';
      }
      res.json(sendData);
    })
    .catch((err) => {
      if (err.message !== 'done') {
        res.json({
          status: 'error',
          message: 'Welcome to Pick N Drop.'
        });
      }
    });
}

export default {
  create,
  verify,
  history,
  detailedHistory,
  rePassword,
  reVerify,
  account,
  update,
  availableDrivers,
  estimation,
  createRequest,
  deleteRequest,
  deleteTrip,
  rate,
  state
};
