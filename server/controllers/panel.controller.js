import httpStatus from 'http-status';
import User from '../models/user.model';
import Driver from '../models/driver.model';
import Trips from '../models/trip.model';
import FinishedTrip from '../models/finishedTrip.model';
import UnfinishedTrip from '../models/unfinishedTrip.model';
import Pole from '../models/pole.model';
import Prepole from '../models/prepole.model';
import Account from '../models/accounts.model';
import SMSHelper from '../helpers/SMSHelper';
import APIError from '../helpers/APIError';

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function dashboard(req, res) {
  let userCount = 0;
  let driverCount = 0;
  let finishedTripCount = 0;
  let unfinishedTripCount = 0;

  User.find({})
    .then((users) => {
      userCount = users.length;
      return Driver.find({});
    })
    .then((drivers) => {
      driverCount = drivers.length;
      return FinishedTrip.find({});
    })
    .then((finishedTrips) => {
      finishedTripCount = finishedTrips.length;
      return UnfinishedTrip.find({});
    })
    .then((unfinishedTrips) => {
      unfinishedTripCount = unfinishedTrips.length;
      res.json({
        status: 'success',
        userCount,
        driverCount,
        finishedTripCount,
        unfinishedTripCount
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function liveMap(req, res) {
  Driver.find({})
    .then((drivers) => {
      res.json({
        status: 'success',
        drivers
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function liveTrips(req, res) {
  Trips.find({})
    .then((trips) => {
      res.json({
        status: 'success',
        trips
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function successTrips(req, res) {
  FinishedTrip.find({})
    .then((finishedTrips) => {
      res.json({
        status: 'success',
        finishedTrips
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function cancelTrips(req, res) {
  UnfinishedTrip.find({})
    .then((unfinishedTrips) => {
      res.json({
        status: 'success',
        unfinishedTrips
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function pole(req, res) {
  Pole.find({})
    .then((poles) => {
      res.json({
        status: 'success',
        poles
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function preRequestPole(req, res) {
  Prepole.find({})
    .then((poles) => {
      res.json({
        status: 'success',
        poles
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function accountsPanel(req, res) {
  let finishedTripsInfo = {};
  let accountInfo = {};
  FinishedTrip.find({})
    .then((finishedTrips) => {
      finishedTripsInfo = finishedTrips;
      return Account.find({});
    })
    .then((account) => {
      accountInfo = account;
      return Driver.find({});
    })
    .then((drivers) => {
      const sortedDrivers = drivers.map(driver => ({
        driverId: driver._id,
        publicId: driver.publicId,
        mobileNumber: driver.mobileNumber,
        name: driver.name,
        vehicleType: driver.vehicle.vehicleType,
        account: driver.account
      }));

      res.json({
        status: 'success',
        sortedDrivers,
        finishedTripsInfo,
        accountInfo
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function driverList(req, res) {
  Driver.find({})
    .then((drivers) => {
      const sortedDrivers = drivers.map(driver => ({
        publicId: driver.publicId,
        name: driver.name,
        mobileNumber: driver.mobileNumber,
        vehicle: driver.vehicle,
        status: driver.status,
        rating: driver.rating,
        warning: driver.warning,
        tripCount: driver.tripCount,
        account: driver.account,
        image: driver.image
      }));
      res.json({
        status: 'success',
        sortedDrivers
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function userList(req, res) {
  User.find({})
    .then((users) => {
      const sortedUsers = users.map(user => ({
        name: user.name,
        image: user.image,
        mobileNumber: user.mobileNumber,
        email: user.email,
        rating: user.rating,
        verification: user.verification,
        device: user.device,
        tripCount: user.tripCount,
        cancelToday: user.cancelToday,
        account: user.account,
        createdAt: user.createdAt
      }));
      res.json({
        status: 'success',
        sortedUsers
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function payment(req, res) {
  let driverInfo = {};
  Driver.get(req.body.driverId)
    .then((driver) => {
      driverInfo = driver;
      if (req.body.balancePaid > driver.account.balance) {
        throw new APIError('Please make sure you entered correct amount.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.account.balance -= req.body.balancePaid;
      saveDriver.account.paid += req.body.balancePaid;
      return saveDriver.save();
    })
    .then(() => {
      const account = new Account({
        publicId: driverInfo.publicId,
        driverId: driverInfo._id,
        mobileNumber: driverInfo.mobileNumber,
        name: driverInfo.name,
        balanceDue: driverInfo.account.balance,
        balancePaid: req.body.balancePaid
      });
      return account.save();
    })
    .then((savedAccount) => {
      res.json({
        status: 'success',
        balancePaid: savedAccount.balancePaid
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns dateTest value
 * @param req
 * @param res
 * @returns {*}
 */
function bulkSms(req, res) {
  const message = req.body.message;
  const receivers = req.body.receiver.replace(/\s/g, '');
  const receiversArray = receivers.split(',');
  receiversArray.map(phoneNumber => SMSHelper.send(phoneNumber, message));
  res.json({
    status: 'success'
  });
}

export default {
  dashboard,
  liveMap,
  liveTrips,
  successTrips,
  cancelTrips,
  pole,
  accountsPanel,
  driverList,
  userList,
  preRequestPole,
  payment,
  bulkSms
};
