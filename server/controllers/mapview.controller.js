import User from '../models/user.model';
import Driver from '../models/driver.model';
import Trip from '../models/trip.model';

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function firstLoad(req, res) {
  let driverInfo = {};
  let userInfo = {};
  Driver.findOne({ publicId: req.body.publicId })
    .then((driver) => {
      driverInfo = driver;
      return User.get(req.body.userId);
    })
    .then((user) => {
      userInfo = user;
      return Trip.findOne({ userId: req.body.userId });
    })
    .then((trip) => {
      const options = {
        driver: {
          id: driverInfo.publicId,
          name: driverInfo.name,
          location: driverInfo.location,
          type: driverInfo.vehicle.vehicleType,
          image: `50.63.165.217:5258/static/drivers/${driverInfo.image}`
        },
        user: {
          id: userInfo._id,
          name: userInfo.name,
          location: userInfo.location
        },
        route: trip.route
      };
      res.json({
        status: 'success',
        options
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function lifetime(req, res) {
  let driverInfo = {};
  Driver.findOne({ publicId: req.body.publicId })
    .then((driver) => {
      driverInfo = driver;
      return User.get(req.body.userId);
    })
    .then((user) => {
      const info = {
        driver: {
          location: driverInfo.location
        },
        user: {
          location: user.location
        },
      };
      res.json({
        status: 'success',
        info
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

export default {
  firstLoad,
  lifetime
};
