import httpStatus from 'http-status';
import moment from 'moment';
import strings from '../../config/strings';
import User from '../models/user.model';
import Driver from '../models/driver.model';
import Warning from '../models/warning.model';
import Request from '../models/request.model';
import Trip from '../models/trip.model';
import Option from '../models/option.model';
import FinishedTrip from '../models/finishedTrip.model';
import UnfinishedTrip from '../models/unfinishedTrip.model';
import IMGHelper from '../helpers/IMGHelper';
import TYMHelper from '../helpers/TYMHelper';
import MAPHelper from '../helpers/MAPHelper';
import FCMHelper from '../helpers/FCMHelper';
import MAILHelper from '../helpers/MAILHelper';
import APIError from '../helpers/APIError';

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res) {
  Driver.getToCheckExists(req.body.mobileNumber)
    .then(() => {
      if (req.body.image) {
        return IMGHelper.driverImage(req.body.image);
      }
      return 'default.png';
    })
    .then((imageName) => {
      const driver = new Driver({
        publicId: req.body.publicId,
        name: req.body.name,
        mobileNumber: req.body.mobileNumber,
        image: imageName,
        password: req.body.password,
        vehicle: {
          vehicleType: req.body.vehicleType,
          vehicleModel: req.body.vehicleModel,
          vehicleNumber: req.body.vehicleNumber
        }
      });
      return driver.save();
    })
    .then((savedDriver) => {
      res.json({
        status: 'success',
        publicId: savedDriver.publicId
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function update(req, res) {
  Driver.get(req.body.driverId)
    .then((driver) => {
      const newTimestamp = parseInt(req.body.timestamp, 10);
      const oldTimestamp = parseInt(driver.location.updateTime, 10);
      if (newTimestamp < oldTimestamp) {
        throw new APIError('Couldn\'t update! Old location.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.location.lat = req.body.lat;
      saveDriver.location.lng = req.body.lng;
      saveDriver.location.updateTime = req.body.timestamp;
      const distance = MAPHelper.getDistance(driver.lastLocation.lat,
        driver.lastLocation.lng, req.body.lat, req.body.lng);
      if (distance >= 1 && driver.status.isOnTrip === false) {
        saveDriver.warning.total.limit += 1;
        saveDriver.warning.today.limit += 1;
        saveDriver.lastLocation.lat = req.body.lat;
        saveDriver.lastLocation.lng = req.body.lng;
        FCMHelper.pushWithoutTag([driver.pushCode], 'Warning', strings.fcmBodyBusy);
      }
      // console.log(saveDriver);
      return saveDriver.save();
    })
    .then((savedDriver) => {
      if (savedDriver.status.isOnTrip === true && savedDriver.status.isPendingBill === false) {
        return Trip.findOne({ driverId: savedDriver._id });
      }
      return 'done';
    })
    .then((trip) => {
      if (trip === 'done') return 'done';
      const tripPath = trip.tripPath;
      const lastPositon = tripPath[tripPath.length - 1].split(',');
      const distance = MAPHelper.getDistance(lastPositon[0],
        lastPositon[1], req.body.lat, req.body.lng);
      if (distance > 0.1) {
        tripPath.push(`${req.body.lat}, ${req.body.lng}`);
      }
      const saveTrip = Object.create(trip);
      saveTrip.tripPath = tripPath;
      return saveTrip.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Location successfully updated.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function pushCode(req, res) {
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isLoggedIn === false) {
        throw new APIError('Can\'t proceed! Please login first.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.status.pushCode = pushCode;
      return saveDriver.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Push code successfully updated.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function active(req, res) {
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isOnTrip === true) {
        throw new APIError('Can\'t proceed! Active trip in progress.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.status.isLoggedIn = true;
      saveDriver.status.isActive = true;
      return saveDriver.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'You are now available for trip.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function inactive(req, res) {
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isOnTrip === true) {
        throw new APIError('Can\'t proceed! Active trip in progress.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.status.isLoggedIn = true;
      saveDriver.status.isActive = false;
      return saveDriver.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'You are now inactive.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Takes data from requestModel and creates Trip.
 * @property {ObjectId} req.body.requestId - The ObjectId of request.
 * @property {ObjectId} req.body.driverId - The ObjectId of driver.
 * @property {ObjectId} req.body.warningId - The ObjectId of warning.
 * @property {ObjectId} req.body.publicId - The publicId of driver.
 * @returns {Trip}
 */
function createTrip(req, res) {
  Warning.get(req.body.warningId)
    .then((warning) => {
      const drivers = warning.drivers;
      const index = drivers.indexOf(req.body.publicId);
      if (index > -1) {
        drivers.splice(index, 1);
      }
      const saveWarning = Object.create(warning);
      saveWarning.drivers = drivers;
      saveWarning.save();
    });
  let userInfo = {};
  let driverInfo = {};
  let requestInfo = {};
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isOnTrip === true) {
        throw new APIError('Can\'t proceed! You are already in a Trip.', httpStatus.NOT_FOUND);
      }
      driverInfo = driver;
      return Request.get(req.body.requestId);
    })
    .then((request) => {
      requestInfo = request;
      return Request.remove({ _id: req.body.requestId });
    })
    .then((response) => {
      if (response.result.n === 0) {
        throw new APIError('Can\'t proceed! Already accepted or cancelled by user.', httpStatus.NOT_FOUND);
      }
      return User.get(requestInfo.userId);
    })
    .then((user) => {
      userInfo = user;
      const saveDriver = Object.create(driverInfo);
      saveDriver.status.isOnTrip = true;
      return saveDriver.save();
    })
    .then(() => {
      const trip = new Trip({
        userId: userInfo._id,
        driverId: driverInfo._id,
        publicId: driverInfo.publicId,
        vehicleType: requestInfo.vehicleType,
        route: requestInfo.route
      });
      return trip.save();
    })
    .then((savedTrip) => {
      const pushData = {
        tripId: savedTrip._id,
        route: requestInfo.route,
        driver: {
          publicId: driverInfo.publicId,
          name: driverInfo.name,
          image: driverInfo.image,
          mobileNumber: driverInfo.mobileNumber,
          rating: driverInfo.rating.value.toFixed(1),
          lat: driverInfo.location.lat,
          lng: driverInfo.location.lng
        },
        vehicle: driverInfo.vehicle
      };
      if (userInfo.device.deviceOs === 'Android') {
        FCMHelper.pushWithoutTag([userInfo.pushCode], 'Accept', pushData);
      } else {
        FCMHelper.pushWithTag([userInfo.pushCode], 'Accept', pushData);
      }
      res.json({
        status: 'success',
        tripId: savedTrip._id,
        route: savedTrip.route,
        user: {
          userId: userInfo._id,
          name: userInfo.name,
          image: userInfo.image,
          mobileNumber: userInfo.mobileNumber,
          rating: userInfo.rating.value.toFixed(1),
          lat: userInfo.location.lat,
          lng: userInfo.location.lng
        }
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {ObjectId} req.body.tripId - The ObjectId of trip.
 * @property {String} req.body.job - The job type of request.
 * @returns {User}
 */
function updateTrip(req, res) {
  const currentTime = new Date();
  let userInfo = {};
  let tripInfo = {};
  let driverInfo = {};
  let successMessage = '';
  Trip.get(req.body.tripId)
    .then((trip) => {
      tripInfo = trip;
      return User.get(trip.userId);
    })
    .then((user) => {
      userInfo = user;
      return Driver.get(tripInfo.driverId);
    })
    .then((driver) => {
      driverInfo = driver;
      const saveTrip = Object.create(tripInfo);
      if (req.body.job === 'Arrive' && tripInfo.tripState === 0) {
        saveTrip.tripTiming.arrive = currentTime;
        saveTrip.tripState = 1;
        successMessage = 'Successfully arrived.';
      } else if (req.body.job === 'Start' && tripInfo.tripState === 1) {
        saveTrip.tripPath.push(`${driver.location.lat}, ${driver.location.lng}`);
        saveTrip.tripTiming.start = currentTime;
        saveTrip.tripState = 2;
        successMessage = 'Successfully started.';
      } else {
        throw new APIError('Can\'t proceed! Please restart your app.', httpStatus.NOT_FOUND);
      }
      return saveTrip.save();
    })
    .then((savedTrip) => {
      const pushData = {
        tripId: savedTrip._id,
        route: savedTrip.route,
        driver: {
          publicId: driverInfo.publicId,
          name: driverInfo.name,
          image: driverInfo.image,
          mobileNumber: driverInfo.mobileNumber,
          rating: driverInfo.rating.value.toFixed(1),
          lat: driverInfo.location.lat,
          lng: driverInfo.location.lng
        },
        vehicle: driverInfo.vehicle
      };
      if (userInfo.device.deviceOs === 'Android') {
        FCMHelper.pushWithoutTag([userInfo.pushCode], req.body.job, pushData);
      } else {
        FCMHelper.pushWithTag([userInfo.pushCode], req.body.job, pushData);
      }
      res.json({
        status: 'success',
        message: successMessage
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function deleteTrip(req, res) {
  let tripInfo = {};
  let driverInfo = {};
  let userInfo = {};
  Trip.get(req.body.tripId)
    .then((trip) => {
      if (trip.tripState === 2) {
        throw new APIError('Can\'t proceed! Trip already started.', httpStatus.NOT_FOUND);
      }
      tripInfo = trip;
      return Trip.remove({ _id: trip._id });
    })
    .then(() => User.get(tripInfo.userId))
    .then((user) => {
      userInfo = user;
      return Driver.get(tripInfo.driverId);
    })
    .then((driver) => {
      driverInfo = driver;
      const saveDriver = Object.create(driver);
      saveDriver.status.isPendingBill = false;
      saveDriver.status.isOnTrip = false;
      saveDriver.status.isActive = false;
      saveDriver.tripCount.today.unfinished += 1;
      saveDriver.tripCount.total.unfinished += 1;
      return saveDriver.save();
    })
    .then(() => {
      const unfinishedTrip = new UnfinishedTrip({
        tripId: tripInfo._id,
        user: {
          userId: userInfo._id,
          mobileNumber: userInfo.mobileNumber,
          name: userInfo.name,
          image: userInfo.image
        },
        driver: {
          driverId: driverInfo.driverId,
          publicId: driverInfo.publicId,
          mobileNumber: driverInfo.mobileNumber,
          name: driverInfo.name,
          image: driverInfo.image
        },
        vehicleType: tripInfo.vehicleType,
        route: tripInfo.route,
        tripTiming: {
          accept: tripInfo.tripTiming.accept,
          arrive: tripInfo.tripTiming.arrive,
          start: tripInfo.tripTiming.start,
          end: tripInfo.tripTiming.end
        },
        tripState: tripInfo.tripState,
        cancelledBy: 'Driver'
      });
      return unfinishedTrip.save();
    })
    .then(() => {
      const pushData = 'Sorry! The trip has been cancelled by the driver.';
      if (userInfo.device.deviceOs === 'Android') {
        FCMHelper.pushWithoutTag([userInfo.pushCode], 'Cancel', pushData);
      } else {
        FCMHelper.pushWithTag([userInfo.pushCode], 'Cancel', pushData);
      }
      res.json({
        status: 'success',
        message: 'Trip cancelled successfully.',
        pushData
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function createFinishTrip(req, res) {
  const currentTime = new Date();
  let userInfo = {};
  let tripInfo = {};
  let driverInfo = {};
  let finishedTripInfo = {};
  Trip.get(req.body.tripId)
    .then((trip) => {
      tripInfo = trip;
      if (trip.tripState !== 2) {
        throw new APIError('Can\'t proceed! Please restart your app.', httpStatus.NOT_FOUND);
      }
      return User.get(trip.userId);
    })
    .then((user) => {
      userInfo = user;
      return Driver.get(tripInfo.driverId);
    })
    .then((driver) => {
      driverInfo = driver;
      return Option.find({});
    })
    .then((options) => {
      let ttPerMin = 3;
      let wtPerMin = 1;
      let baseFare = 25;
      if (tripInfo.vehicleType === 'bike') {
        ttPerMin = options[0].billing.bike.ttPerMin;
        wtPerMin = options[0].billing.bike.wtPerMin;
        baseFare = options[0].billing.bike.baseFare;
      } else if (tripInfo.vehicleType === 'car') {
        ttPerMin = options[0].billing.car.ttPerMin;
        wtPerMin = options[0].billing.car.wtPerMin;
        baseFare = options[0].billing.car.baseFare;
      }
      const _tm = TYMHelper.getMinutes(tripInfo.tripTiming.start, currentTime);
      const _wm = TYMHelper.getMinutes(tripInfo.tripTiming.arrive, tripInfo.tripTiming.start);

      const _ttBill = _tm * ttPerMin;
      const _wtBill = _wm * wtPerMin;
      const _subBill = +_ttBill + _wtBill + baseFare;

      const subTotalBill = Math.round((_subBill / 100) * (100 - options[0].discount.value));
      const roundDown = subTotalBill % 5;
      const totalBill = subTotalBill - roundDown;

      const fromPosition = `${tripInfo.route.from.lat}, ${tripInfo.route.from.lng}`;
      const toPositon = `${driverInfo.location.lat}, ${driverInfo.location.lng}`;
      const tripPath = MAPHelper.getTripPath(tripInfo.tripPath);
      tripPath.push(toPositon);
      const snapshot = MAPHelper.getTripImage(fromPosition, toPositon, tripPath);

      const finishedTrip = new FinishedTrip({
        tripId: tripInfo._id,
        user: {
          userId: userInfo._id,
          mobileNumber: userInfo.mobileNumber,
          name: userInfo.name,
          image: userInfo.image
        },
        driver: {
          driverId: driverInfo._id,
          publicId: driverInfo.publicId,
          mobileNumber: driverInfo.mobileNumber,
          name: driverInfo.name,
          image: driverInfo.image
        },
        vehicleType: tripInfo.vehicleType,
        route: tripInfo.route,
        tripTiming: {
          accept: tripInfo.tripTiming.accept,
          arrive: tripInfo.tripTiming.arrive,
          start: tripInfo.tripTiming.start,
          end: currentTime
        },
        tripPath,
        billing: {
          totalBill,
          subTotalBill,
          roundDown,
          baseFare,
          ttPerMin,
          wtPerMin,
          tripTime: _tm,
          tripTimeBill: _ttBill,
          waitingTime: _wm,
          waitingTimeBill: _wtBill,
          discount: options[0].discount.value
        },
        snapshot
      });
      return finishedTrip.save();
    })
    .then((savedFinishedTrip) => {
      finishedTripInfo = savedFinishedTrip;
      return Trip.remove({ _id: tripInfo._id });
    })
    .then(() => {
      const saveDriver = Object.create(driverInfo);
      saveDriver.status.isPendingBill = true;
      return saveDriver.save();
    })
    .then(() => {
      const pushData = {
        successId: finishedTripInfo._id,
        route: finishedTripInfo.route,
        billing: finishedTripInfo.billing,
        snapshot: finishedTripInfo.snapshot,
        driver: {
          publicId: driverInfo.publicId,
          name: driverInfo.name,
          image: driverInfo.image,
          mobileNumber: driverInfo.mobileNumber,
          rating: driverInfo.rating.value.toFixed(1),
          lat: driverInfo.location.lat,
          lng: driverInfo.location.lng
        },
        vehicle: driverInfo.vehicle
      };
      if (userInfo.device.deviceOs === 'Android') {
        FCMHelper.pushWithoutTag([userInfo.pushCode], 'End', pushData);
      } else {
        FCMHelper.pushWithTag([userInfo.pushCode], 'End', pushData);
      }
      const receiptBody = {
        successId: finishedTripInfo._id,
        route: finishedTripInfo.route,
        billing: finishedTripInfo.billing,
        snapshot: finishedTripInfo.snapshot,
        userName: userInfo.name
      };
      MAILHelper.sendReceipt(userInfo.email, receiptBody);
      res.json({
        status: 'success',
        message: 'Successfully ended trip.',
        finishTripId: finishedTripInfo._id,
        totalBill: finishedTripInfo.billing.totalBill,
        discount: finishedTripInfo.billing.discount,
        route: finishedTripInfo.route,
        user: {
          userId: userInfo._id,
          name: userInfo.name,
          image: userInfo.image,
          mobileNumber: userInfo.mobileNumber,
          rating: userInfo.rating.value.toFixed(1),
          lat: userInfo.location.lat,
          lng: userInfo.location.lng
        }
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Change driver status to active
 * @property {string} req.body.driverId - The driverId of driver.
 * @returns {User}
 */
function updateFinishTrip(req, res) {
  const currentDate = new Date();
  let finishedTripInfo = {};
  let userInfo = {};
  FinishedTrip.get(req.body.finishTripId)
    .then((finishedTrip) => {
      if (finishedTrip.billing.isPaid === true) {
        throw new APIError('Bill already taken. Please restart your app.', httpStatus.NOT_FOUND);
      }
      if (req.body.method !== 'Account' && req.body.method !== 'Cash') {
        throw new APIError('Payment method is not set.', httpStatus.NOT_FOUND);
      }
      finishedTripInfo = finishedTrip;
      return User.get(finishedTrip.user.userId);
    })
    .then((user) => {
      userInfo = user;
      const saveUser = Object.create(user);
      if (req.body.method === 'Account') {
        if (user.account.balance < finishedTripInfo.billing.totalBill) {
          throw new APIError('Not enough balance! Please ask passenger to recharge.', httpStatus.NOT_FOUND);
        }
        saveUser.account.balance -= finishedTripInfo.billing.totalBill;
        saveUser.account.spent += finishedTripInfo.billing.totalBill;
      }
      saveUser.tripCount.finished += 1;
      saveUser.cancelToday.count = 0;
      saveUser.cancelToday.date = currentDate;
      return saveUser.save();
    })
    .then(() => Driver.get(finishedTripInfo.driver.driverId))
    .then((driver) => {
      const saveDriver = Object.create(driver);
      if (req.body.method === 'Cash') {
        saveDriver.account.balance += finishedTripInfo.billing.totalBill;
      }
      saveDriver.tripCount.today.finished += 1;
      saveDriver.tripCount.total.finished += 1;
      saveDriver.status.isActive = false;
      saveDriver.status.isOnTrip = false;
      saveDriver.status.isPendingBill = false;
      saveDriver.lastLocation.lat = driver.location.lat;
      saveDriver.lastLocation.lng = driver.location.lng;
      return saveDriver.save();
    })
    .then(() => {
      const saveFinishedTrip = Object.create(finishedTripInfo);
      saveFinishedTrip.billing.isPaid = true;
      return saveFinishedTrip.save();
    })
    .then(() => {
      const pushData = 'Payment received. Thank you for using Pick N Drop.';
      if (userInfo.device.deviceOs === 'Android') {
        FCMHelper.pushWithoutTag([userInfo.pushCode], 'Payment', pushData);
      } else {
        FCMHelper.pushWithTag([userInfo.pushCode], 'Payment', pushData);
      }
      res.json({
        status: 'success',
        message: 'Payment received. Best of luck for the next trip.',
        pushData
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Rate user
 * @property {ObjectId} req.body.finishTripId - The _id of finishedTrip.
 * @property {String} req.body.userId - The _id of user.
 * @property {Number} req.body.rating - The rating value for user.
 * @returns {User}
 */
function rate(req, res) {
  let finishedTripInfo = {};
  FinishedTrip.get(req.body.finishTripId)
    .then((finishedTrip) => {
      finishedTripInfo = finishedTrip;
      const saveFinishedTrip = Object.create(finishedTrip);
      saveFinishedTrip.rating.driver = req.body.rating;
      return saveFinishedTrip.save();
    })
    .then(() => User.get(finishedTripInfo.user.userId))
    .then((user) => {
      const saveUser = Object.create(user);
      const rating = ((user.rating.value * user.rating.count)
        + req.body.rating) / (user.rating.count + 1);
      saveUser.rating.count += 1;
      saveUser.rating.value = rating;
      return saveUser.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Rating success. Best of luck for next trip.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Get state
 * @property {ObjectId} req.body.driverId - The _id of driver.
 * @returns {User}
 */
function state(req, res) {
  let responseInfo = {};
  let responseType = '';
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isLoggedIn === false) {
        res.json({
          status: 'Logout',
          message: 'Please re-login to your account.'
        });
        throw new APIError('done', httpStatus.NOT_FOUND);
      } else if (driver.status.isPendingBill === true) {
        responseType = 'finishTrip';
        return FinishedTrip.findOne({ 'driver.driverId': req.body.driverId }, {}, { sort: { createdAt: -1 } });
      } else if (driver.status.isOnTrip === true) {
        responseType = 'trip';
        return Trip.findOne({ driverId: req.body.driverId });
      }
      const saveDriver = Object.create(driver);
      const currentDate = new Date();
      const newDate = moment(currentDate).format('YYYY-MM-DD');
      let oldDate = moment(driver.tripCount.today.date).format('YYYY-MM-DD');
      if (moment(newDate).isAfter(oldDate)) {
        saveDriver.tripCount.today.date = currentDate;
        saveDriver.tripCount.today.finished = 0;
        saveDriver.tripCount.today.unfinished = 0;
      }
      oldDate = moment(driver.warning.today.date).format('YYYY-MM-DD');
      if (moment(newDate).isAfter(oldDate)) {
        saveDriver.warning.today.date = currentDate;
        saveDriver.warning.today.limit = 0;
        saveDriver.warning.today.accept = 0;
        saveDriver.warning.today.appKill = 0;
      }
      let warning = saveDriver.warning.today.appKill;
      warning += saveDriver.warning.today.accept + saveDriver.warning.today.limit;
      saveDriver.save();
      res.json({
        warning,
        status: 'success',
        driverId: saveDriver._id,
        publicId: saveDriver.publicId,
        name: saveDriver.name,
        image: saveDriver.image,
        rating: saveDriver.rating.value.toFixed(1),
        vehicleType: saveDriver.vehicle.vehicleType,
        vehicleModel: saveDriver.vehicle.vehicleModel,
        vehicleNumber: saveDriver.vehicle.vehicleNumber,
        tripCount: saveDriver.tripCount.today.finished
      });
      throw new APIError('done', httpStatus.NOT_FOUND);
    })
    .then((response) => {
      responseInfo = response;
      if (responseType === 'trip') {
        return User.get(response.userId);
      }
      return User.get(response.user.userId);
    })
    .then((user) => {
      if (responseType === 'trip') {
        const sendData = {
          status: 'Accept',
          tripId: responseInfo._id,
          route: responseInfo.route,
          user: {
            userId: user._id,
            name: user.name,
            image: user.image,
            mobileNumber: user.mobileNumber,
            rating: user.rating.value.toFixed(1),
            lat: user.location.lat,
            lng: user.location.lng
          }
        };
        if (responseInfo.tripState === 0) {
          sendData.status = 'Accept';
        } else if (responseInfo.tripState === 1) {
          sendData.status = 'Arrive';
        } else if (responseInfo.tripState === 2) {
          sendData.status = 'Start';
        }
        res.json(sendData);
      } else {
        const sendData = {
          status: 'End',
          finishTripId: responseInfo._id,
          totalBill: responseInfo.billing.totalBill,
          discount: responseInfo.billing.discount,
          route: responseInfo.route,
          user: {
            userId: user._id,
            name: user.name,
            image: user.image,
            mobileNumber: user.mobileNumber,
            rating: user.rating.value.toFixed(1),
            lat: user.location.lat,
            lng: user.location.lng
          }
        };
        res.json(sendData);
      }
    })
    .catch((err) => {
      if (err.message !== 'done') {
        res.json({
          status: 'error',
          message: err.message
        });
      }
    });
}

export default {
  create,
  update,
  pushCode,
  active,
  inactive,
  createTrip,
  updateTrip,
  deleteTrip,
  createFinishTrip,
  updateFinishTrip,
  rate,
  state
};
