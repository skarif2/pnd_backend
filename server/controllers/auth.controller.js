import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import bcrypt from 'bcryptjs';
import moment from 'moment';
import User from '../models/user.model';
import Driver from '../models/driver.model';
import APIError from '../helpers/APIError';
import config from '../../config/config';

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @returns {*}
 */
function userLogin(req, res) {
  User.getByMobileNumber(req.body.mobileNumber)
    .then((user) => {
      if (user.verification.verified === false) {
        return res.json({
          status: 'verify',
          mobileNumber: user.mobileNumber
        });
      }
      if (bcrypt.compareSync(req.body.password, user.password)) {
        const saveUser = Object.create(user);
        saveUser.pushCode = req.body.pushCode;
        saveUser.device.deviceId = req.body.deviceId;
        saveUser.device.deviceOs = req.body.deviceOs;
        return saveUser.save();
      }
      throw new APIError('Mobile number or password did not match.', httpStatus.NOT_FOUND);
    })
    .then((savedUser) => {
      if (savedUser._id) {
        const token = jwt.sign({
          mobileNumber: savedUser.mobileNumber
        }, config.jwtSecret);
        res.json({
          token,
          status: 'success',
          userId: savedUser._id,
          name: savedUser.name,
          mobileNumber: savedUser.mobileNumber,
          email: savedUser.email,
          image: savedUser.image
        });
      }
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
* Returns success if valid deviceId is provided
* @param req
* @param res
* @returns {*}
*/
function userLogout(req, res) {
  User.get(req.body.userId)
    .then((user) => {
      if (user.device.deviceId === req.body.deviceId) {
        const saveUser = Object.create(user);
        saveUser.pushCode = '';
        saveUser.device.deviceId = '';
        saveUser.device.deviceOs = '';
        return saveUser.save();
      }
      throw new APIError('Error! Please close and reopen you app.', httpStatus.NOT_FOUND);
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Successfully logged out.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @returns {*}
 */
function driverLogin(req, res) {
  let responseObject = {};
  Driver.getByMobileNumber(req.body.mobileNumber)
    .then((driver) => {
      if (req.body.password === driver.password) {
        const saveDriver = Object.create(driver);
        const currentDate = new Date();
        const newDate = moment(currentDate).format('YYYY-MM-DD');
        let oldDate = moment(driver.tripCount.today.date).format('YYYY-MM-DD');
        if (moment(newDate).isAfter(oldDate)) {
          saveDriver.tripCount.today.date = currentDate;
          saveDriver.tripCount.today.finished = 0;
          saveDriver.tripCount.today.unfinished = 0;
        }
        oldDate = moment(driver.warning.today.date).format('YYYY-MM-DD');
        if (moment(newDate).isAfter(oldDate)) {
          saveDriver.warning.today.date = currentDate;
          saveDriver.warning.today.limit = 0;
          saveDriver.warning.today.accept = 0;
          saveDriver.warning.today.appKill = 0;
        }
        saveDriver.pushCode = req.body.pushCode;
        saveDriver.status.isLoggedIn = true;
        let warning = saveDriver.warning.today.appKill;
        warning += saveDriver.warning.today.accept + saveDriver.warning.today.limit;
        responseObject = {
          warning,
          status: 'success',
          driverId: saveDriver._id,
          publicId: saveDriver.publicId,
          name: saveDriver.name,
          image: saveDriver.image,
          rating: saveDriver.rating.value.toFixed(1),
          vehicleType: saveDriver.vehicle.vehicleType,
          vehicleModel: saveDriver.vehicle.vehicleModel,
          vehicleNumber: saveDriver.vehicle.vehicleNumber,
          tripCount: saveDriver.tripCount.today.finished
        };
        return saveDriver.save();
      }
      throw new APIError('Mobile number or password did not match.', httpStatus.NOT_FOUND);
    })
    .then(() => {
      res.json(responseObject);
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

/**
 * Returns success if valid deviceId is provided
 * @param req
 * @param res
 * @returns {*}
 */
function driverLogout(req, res) {
  Driver.get(req.body.driverId)
    .then((driver) => {
      if (driver.status.isOnTrip === true) {
        throw new APIError('Can\'t logout! Active trip in progress.', httpStatus.NOT_FOUND);
      }
      const saveDriver = Object.create(driver);
      saveDriver.status.isLoggedIn = false;
      saveDriver.status.isActive = false;
      return saveDriver.save();
    })
    .then(() => {
      res.json({
        status: 'success',
        message: 'Successfully logged out.'
      });
    })
    .catch((err) => {
      res.json({
        status: 'error',
        message: err.message
      });
    });
}

export default { userLogin, userLogout, driverLogin, driverLogout };
