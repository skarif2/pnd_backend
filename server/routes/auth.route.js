import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import authCtrl from '../controllers/auth.controller';

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/auth/userLogin - Returns user info if correct mobileNumber and password is provided */
router.route('/userLogin')
  .post(validate(paramValidation.userLogin), authCtrl.userLogin);

/** POST /api/auth/userLogout - Returns success if correct deviceId is provided */
router.route('/userLogout')
  .post(validate(paramValidation.userLogout), authCtrl.userLogout);

/** POST /api/auth/userLogin - Returns user info if correct mobileNumber and password is provided */
router.route('/driverLogin')
  .post(validate(paramValidation.driverLogin), authCtrl.driverLogin);

/** POST /api/auth/userLogout - Returns success if correct deviceId is provided */
router.route('/driverLogout')
  .post(validate(paramValidation.driverLogout), authCtrl.driverLogout);

export default router;
