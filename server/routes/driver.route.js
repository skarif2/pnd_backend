import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import driverCtrl from '../controllers/driver.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .post(validate(paramValidation.createDriver), driverCtrl.create)
  .put(validate(paramValidation.updateDriver), driverCtrl.update);

router.route('/pushCode')
  .put(validate(paramValidation.pushCode), driverCtrl.pushCode);

router.route('/active')
  .post(validate(paramValidation.active), driverCtrl.active);

router.route('/inactive')
  .post(validate(paramValidation.inactive), driverCtrl.inactive);

router.route('/trip')
  .post(validate(paramValidation.createTrip), driverCtrl.createTrip)
  .put(validate(paramValidation.updateTrip), driverCtrl.updateTrip)
  .delete(validate(paramValidation.deleteTrip), driverCtrl.deleteTrip);

router.route('/finishTrip')
  .post(validate(paramValidation.createFinishTrip), driverCtrl.createFinishTrip)
  .put(validate(paramValidation.updateFinishTrip), driverCtrl.updateFinishTrip);

router.route('/rate')
  .put(validate(paramValidation.rate), driverCtrl.rate);

router.route('/state')
  .post(validate(paramValidation.driverState), driverCtrl.state);

export default router;
