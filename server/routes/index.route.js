import express from 'express';
import userRoutes from './user.route';
import driverRoutes from './driver.route';
import authRoutes from './auth.route';
import mapview from './mapview.route';
import panel from './panel.route';
import testRoutes from './test.route';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);

// mount user routes at /drivers
router.use('/drivers', driverRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount mapview routes at /mapview
router.use('/mapview', mapview);


// mount panel routes at /panel
router.use('/panel', panel);

// mount test routes at /test
router.use('/test', testRoutes);

export default router;
