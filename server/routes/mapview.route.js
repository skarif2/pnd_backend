import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import mapviewCtrl from '../controllers/mapview.controller';

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/mapview/firstLoad */
router.route('/firstLoad')
  .post(validate(paramValidation.mapview), mapviewCtrl.firstLoad);

/** POST /api/mapview/lifetime */
router.route('/lifetime')
  .post(validate(paramValidation.mapview), mapviewCtrl.lifetime);

export default router;
