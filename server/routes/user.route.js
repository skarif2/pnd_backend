import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import userCtrl from '../controllers/user.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .post(validate(paramValidation.createUser), userCtrl.create)
  .put(validate(paramValidation.updateUser), userCtrl.update);

router.route('/verify')
  .post(validate(paramValidation.verifyUser), userCtrl.verify);

router.route('/history')
  .post(validate(paramValidation.history), userCtrl.history);

router.route('/rePassword')
  .post(validate(paramValidation.rePassword), userCtrl.rePassword);

router.route('/reVerify')
  .post(validate(paramValidation.reVerifyUser), userCtrl.reVerify);

router.route('/account')
  .post(validate(paramValidation.userAccount), userCtrl.account);

router.route('/detailedHistory')
  .post(validate(paramValidation.detailedHistory), userCtrl.detailedHistory);

router.route('/available')
  .post(validate(paramValidation.availableDrivers), userCtrl.availableDrivers);

router.route('/estimation')
  .post(validate(paramValidation.estimation), userCtrl.estimation);

router.route('/request')
  .post(validate(paramValidation.createRequest), userCtrl.createRequest)
  .delete(validate(paramValidation.deleteRequest), userCtrl.deleteRequest);

router.route('/trip')
  .delete(validate(paramValidation.deleteTrip), userCtrl.deleteTrip);

router.route('/rate')
  .put(validate(paramValidation.rate), userCtrl.rate);

router.route('/state')
  .post(validate(paramValidation.userState), userCtrl.state);

export default router;
