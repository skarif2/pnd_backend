import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import panelCtrl from '../controllers/panel.controller';

const router = express.Router(); // eslint-disable-line new-cap

/** get /api/panel/dashboard */
router.route('/dashboard')
  .get(panelCtrl.dashboard);

/** get /api/panel/liveTrips */
router.route('/liveTrips')
  .get(panelCtrl.liveTrips);

/** get /api/panel/liveMap */
router.route('/liveMap')
  .get(panelCtrl.liveMap);

/** get /api/panel/finishedTrips */
router.route('/finishedTrips')
  .get(panelCtrl.successTrips);

/** get /api/panel/unfinishedTrips */
router.route('/unfinishedTrips')
  .get(panelCtrl.cancelTrips);

/** get /api/panel/pole */
router.route('/pole')
  .get(panelCtrl.pole);

/** get /api/panel/preRequestPole */
router.route('/preRequestPole')
  .get(panelCtrl.preRequestPole);

/** get /api/panel/accountsPanel */
router.route('/accountsPanel')
  .get(panelCtrl.accountsPanel);

/** get /api/panel/driverList */
router.route('/driverList')
  .get(panelCtrl.driverList);

/** get /api/panel/userList */
router.route('/userList')
  .get(panelCtrl.userList);

/** get /api/panel/payment */
router.route('/payment')
  .post(validate(paramValidation.payment), panelCtrl.payment);

/** get /api/panel/bulkSms */
router.route('/bulkSms')
  .post(validate(paramValidation.bulkSms), panelCtrl.bulkSms);

export default router;
