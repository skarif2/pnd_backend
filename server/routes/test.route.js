import express from 'express';
import testCtrl from '../controllers/test.controller';

const router = express.Router(); // eslint-disable-line new-cap

/** POST /api/test/dateTest */
router.route('/dateTest')
  .post(testCtrl.dateTest);

/** POST /api/test/timeTest */
router.route('/timeTest')
  .post(testCtrl.timeTest);

/** POST /api/test/testPush */
router.route('/testPush')
  .post(testCtrl.testPush);

/** POST /api/test/testPush */
router.route('/testPushUser')
  .post(testCtrl.testPushUser);

/** POST /api/test/testSMS */
router.route('/testSMS')
  .post(testCtrl.testSMS);

export default router;
