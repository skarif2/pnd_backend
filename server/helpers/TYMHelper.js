import moment from 'moment';

function getMinutes(startTime, endTime) {
  const ms = moment(endTime, 'YYYY-MM-DD HH:mm:ss').diff(moment(startTime, 'YYYY-MM-DD HH:mm:ss'));
  const duration = moment.duration(ms, 'milliseconds');
  return Math.floor(duration.asMinutes());
}

export default {
  getMinutes
};
