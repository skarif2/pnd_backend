import Promise from 'bluebird';
import request from 'request';
import xml2js from 'xml2js';
import config from '../../config/config';

const parseString = xml2js.parseString;
const options = {
  user: config.ssl.username,
  pass: config.ssl.password,
  sid: config.ssl.serviceId
};

function _send(receiver, message, cb) {
  options['sms[0][0]'] = receiver;
  options['sms[0][1]'] = message;
  options['sms[0][2]'] = new Date().getTime();

  const requestOptions = {
    uri: 'http://sms.sslwireless.com/pushapi/dynamic/server.php',
    method: 'POST',
    formData: options,
    json: true
  };
  request(requestOptions, (err, res, resBody) => {
    if (err) {
      return cb(err);
    }
    return parseString(resBody, { trim: true }, (error, result) => {
      cb(null, result.REPLY);
    });
  });
}

function send(receiver, message) {
  return new Promise((resolve, reject) => {
    _send(receiver, message, (err, response) => {
      if (err) reject('failed to send sms.');
      else if (response.PARAMETER && response.PARAMETER[0] === 'OK') resolve(response);
      else reject('failed to send sms.');
    });
  });
}

function sendPassword(receiver, password) {
  return new Promise((resolve, reject) => {
    const message = `Your new password is: ${password}. Thank you for using Pick N Drop.`;
    _send(receiver, message, (err, response) => {
      if (err) reject('failed to send sms.');
      else if (response.PARAMETER && response.PARAMETER[0] === 'OK') resolve(response);
      else reject('failed to send sms.');
    });
  });
}

function sendVerification(receiver, code) {
  return new Promise((resolve, reject) => {
    const message = `Verification code: ${code}. Thank you for using Pick N Drop.`;
    _send(receiver, message, (err, response) => {
      if (err) reject('failed to send sms.');
      else if (response.PARAMETER && response.PARAMETER[0] === 'OK') resolve(response);
      else reject('failed to send sms.');
    });
  });
}

export default { send, sendPassword, sendVerification };
