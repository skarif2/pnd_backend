import Promise from 'bluebird';
import FileSystem from 'fs';
import RNDHelper from './RNDHelper';

function userImage(image) {
  let base64image = image;
  return new Promise((resolve, reject) => {
    let imageExtension = 'jpg';
    if (image.charAt(0) === '/') {
      imageExtension = 'jpeg';
    } else if (image.charAt(0) === 'R') {
      imageExtension = 'gif';
    } else if (image.charAt(0) === 'i') {
      imageExtension = 'png';
    }

    const imageName = `${RNDHelper.imageName()}.${imageExtension}`;
    const path = `${__dirname}/../../images/users/${imageName}`;
    base64image = image.replace(/ /g, '+');

    FileSystem.writeFile(path, base64image, 'base64', (err) => {
      if (err) reject(err);
      else resolve(imageName);
    });
  });
}

function driverImage(image) {
  let base64image = image;
  return new Promise((resolve, reject) => {
    const imageExtension = image.match(/^data:image\/(.*);/);
    const imageName = `${RNDHelper.imageName()}.${imageExtension[1]}`;
    const path = `${__dirname}/../../images/drivers/${imageName}`;
    base64image = image.replace(/^(.*)base64,/, '');

    FileSystem.writeFile(path, base64image, 'base64', (err) => {
      if (err) reject(err);
      else resolve(imageName);
    });
  });
}

function deleteImage(imageName) {
  const path = `${__dirname}/../../images/users/${imageName}`;
  return new Promise((resolve, reject) => {
    FileSystem.unlink(path, (err) => {
      if (err) reject(err);
      else resolve(imageName);
    });
  });
}

export default { userImage, driverImage, deleteImage };
