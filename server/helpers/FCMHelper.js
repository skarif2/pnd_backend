import fcm from 'node-gcm';
import request from 'request';
import config from '../../config/config';
import strings from '../../config/strings';

const pushSender = new fcm.Sender(config.fcmPushKey);
const message = new fcm.Message({
  collapseKey: 'Pick N Drop',
  priority: 'high',
  contentAvailable: true,
  delayWhileIdle: false,
  timeToLive: 1200,
  dryRun: false,
  data: {},
  notification: {
    priority: 'high',
    title: 'Pick N Drop',
    icon: 'ic_launcher',
    sound: 'default',
    time_to_live: '2419200',
    delay_while_idle: 'false',
    content_available: 'true',
    body: strings.fcmBody
  }
});


function getBody(state, messages) {
  let body = '';
  switch (state) {
    case 'Request': {
      let from = 'here';
      let to = '';
      if (messages.from) {
        const fromArray = messages.from.split(',');
        if (fromArray[1]) {
          from = `${fromArray[0]}, ${fromArray[1]}`;
        } else if (fromArray[0]) {
          from = fromArray[0];
        }
      }
      if (messages.to) {
        const toArray = messages.to.split(',');
        if (toArray[1]) {
          to = `${toArray[0]}, ${toArray[1]}`;
        } else if (toArray[0]) {
          to = toArray[0];
        }
      }
      body = `Request!! Pick: ${from} || Drop: ${to}`;
      break;
    }
    case 'Accept':
      body = strings.fcmBodyAccept;
      break;
    case 'Arrive':
      body = strings.fcmBodyArrive;
      break;
    case 'Start':
      body = strings.fcmBodyStart;
      break;
    case 'End':
      body = strings.fcmBodyEnd;
      break;
    case 'Busy':
      body = strings.fcmBodyBusy;
      break;
    case 'Cancel':
      body = strings.fcmBodyCancel;
      break;
    case 'Warning':
      body = strings.fcmBodyWarning;
      break;
    case 'Payment':
      body = messages;
      break;
    case 'Test':
      body = 'Test message from server';
      break;
    default:
      body = strings.fcmBody;
  }
  return body;
}

function pushWithTag(regTokens, state, messages) {
  const body = getBody(state, messages);
  message.addNotification('body', body);
  message.addData('message', messages);
  message.addData('state', state);
  pushSender.send(message, { registrationTokens: regTokens }, 10);
  // pushSender.send(message, { registrationTokens: regTokens }, 10, (err, response) => {
  //   if (err) {
  //     console.log(err);
  //   }
  //   console.log(response);
  // });
}

function pushWithoutTag(regTokens, state, messages) {
  const body = getBody(state, messages);
  const options = {
    registration_ids: regTokens,
    priority: 'high',
    data: {
      state,
      body,
      title: 'Pick N Drop',
      message: messages
    }
  };
  const requestOptions = {
    method: 'POST',
    preambleCRLF: true,
    postambleCRLF: true,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `key=${config.fcmPushKey}`
    },
    uri: 'https://fcm.googleapis.com/fcm/send',
    json: options
  };
  request(requestOptions);
  // request(requestOptions, (error, response, resBodyJSON) => {
  //   if (error) {
  //     console.log(error);
  //   }
  //   console.log(resBodyJSON);
  // });
}

function bulkPush(regTokens, messages) {
  message.addData('message', messages);
  message.addData('state', 'Test');
  message.addNotification('body', messages);

  pushSender.send(message, { registrationTokens: regTokens }, 10);
  // pushSender.send(message, { registrationTokens: regTokens }, 10, (err, response) => {
  //   if (err) {
  //     console.log(err);
  //   }
  //   console.log(response);
  // });
}

export default {
  pushWithTag,
  pushWithoutTag,
  bulkPush
};
