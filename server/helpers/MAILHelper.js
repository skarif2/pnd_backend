import nodemailer from 'nodemailer';
import config from '../../config/config';

const receiptTransport = nodemailer.createTransport({
  host: 'mail.pickndrop.com.bd',
  port: 465,
  secure: true, // use TLS
  auth: {
    user: config.env.receiptEmail,
    pass: config.env.receiptPass
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});

const noreplyTransport = nodemailer.createTransport({
  host: 'mail.pickndrop.com.bd',
  port: 465,
  secure: true, // use TLS
  auth: {
    user: config.env.infoEmail,
    pass: config.env.infoPass
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});

function getReceiptBody(tripInfo) {
  return `<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ededed" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;border-collapse:collapse!important;background:#ededed!important;border-collapse:collapse;background:#ededed">
    <tbody>
      <tr>
        <td align="center" valign="top" bgcolor="#2F4159" style="background:#2F4159!important;background:#2F4159">
          <table width="700" bgcolor="#2F4159" border="0" cellspacing="0" cellpadding="0" class="m_-6246123157711681189mobilewrapper" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;background:#2F4159;border-collapse:collapse!important">
            <tbody>
              <tr>
                <td height="10" align="left" valign="top" bgcolor="#2F4159" style="height:10px!important;font-size:10px!important;padding:0px!important;color:#2F4159">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;border-collapse:collapse!important;font-size:18px!important;height:18px;line-height:18px;color:#2F4159">
                    <tbody>
                      <tr>
                        <td bgcolor="#2F4159" style="height:18px;line-height:18px;font-size:18px;color:#2F4159;background:#2F4159">.</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="background:#2F4159">
                  <table width="620" bgcolor="#2F4159" border="0" cellspacing="0" cellpadding="0" class="m_-6246123157711681189mobilewrapper" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;background:#2F4159;border-collapse:collapse!important">
                    <tbody>
                      <tr>
                        <td width="10" align="left" valign="middle" bgcolor="#2F4159" style="width:10px;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:10px;color:#2F4159;background:#2F4159">.</td>
                        <td width="155" align="left" valign="top" style="width:155px">
                          <a href="http://pickndrop.com.bd" style="border:none;outline:none;text-decoration:none" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/logo-OyQPOWZmajTO4oc.png" width="155" style="width:155px;vertical-align:top;border:none;outline:none;text-decoration:none" class="CToWUd"></a>
                        </td>
                        <td width="295" align="left" valign="top" style="width:295px">&nbsp;</td>
                        <td width="10" align="left" valign="middle" bgcolor="#2F4159" style="width:10px;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:10px;color:#2F4159;background:#2F4159">.</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="10" align="left" valign="top" bgcolor="#2F4159" style="height:10px!important;font-size:10px!important;padding:0px!important;color:#2F4159">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;border-collapse:collapse!important;font-size:18px!important;height:18px;line-height:18px;color:#2F4159">
                    <tbody>
                      <tr>
                        <td bgcolor="#2F4159" style="height:18px;line-height:18px;font-size:18px;color:#2F4159;background:#2F4159">.</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#ededed">
          <table width="700" class="m_-6246123157711681189mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table width="600" class="m_-6246123157711681189mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                    <tbody>
                      <tr>
                        <td height="225" align="center" valign="top" style="height:225px!important;line-height:225px!important;font-size:225px!important;color:#ededed!important;height:225px;line-height:225px;font-size:225px;color:#ededed">
  <!-- Trip Image ============================================================ -->
                          <img src="${tripInfo.snapshot}" width="600" style="width:600px;vertical-align:top;border:none;outline:none;text-decoration:none" class="CToWUd">
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top">
                          <table width="600" border="0" class="m_-6246123157711681189mobilewrapper" bgcolor="#ffffff" cellspacing="0" cellpadding="0" style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif">
                            <tbody>
                              <tr>
                                <td width="25" align="left" valign="top" bgcolor="#ffffff" style="background:#ffffff">&nbsp;</td>
                                <td align="left" valign="top" bgcolor="#ffffff" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;background:#ffffff">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;background:#ffffff;font-family:'Open Sans',Arial,Helvetica,sans-serif">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top">&nbsp;</td>
                                      </tr>
                                      <tr>
  <!-- Total Bill ======================================================================================================================================== -->
                                        <td align="center" valign="top" style="font-size:36px;font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#1c2c3a">
                                        ${tripInfo.billing.totalBill} Tk.
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="center" valign="top" style="font-size:15px;font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#1c2c3a">Trip ID
  <!-- Trip Id ========================================================================================================================================== -->
                                          <span style="color:#69C5D4">${tripInfo.successId}</span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="20" valign="top"></td>
                                      </tr>
                                      <td height="1" class="m_-8177750052882491647tronhr" style="background-color:#69C5D4;font-size:0px;line-height:0px" bgcolor="#69C5D4"><img src="https://ci6.googleusercontent.com/proxy/bjlmwp1lfIp2fevkoEO9P7LRLJDB3htzJ41P3i-rSE5ZwtA6fw0XQ__C7XIfeKMxLCjsuN8uGnjTZ77lPg_2Q3X-nIE1drc87j_Mf3u2_Iy1exK-tg=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/no_collapse.png" style="outline:none;text-decoration:none;display:block" class="CToWUd">
                                      </td>
                                      <tr>
                                        <td height="20" valign="top"></td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top">
                                          <font style="font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#212121;line-height:20px">
  <!-- Rider Name ======================================================================================================================================= -->
                                            Hi ${tripInfo.userName},<br>
                                            Thanks for using Pick N Drop. Hope you had a great trip.<br></font>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="20" valign="top"></td>
                                      </tr>
                                      <tr>
                                        <td valign="top">
                                          <img src="http://pickndrop.com.bd/public_profile/public_images/pickup-0U3yyyGE2NpdoE4.png" height = "10" width="10" style="width:10;vertical-align:center;margin-right:10px;border:none;outline:none;text-decoration:none" class="CToWUd">
  <!-- From Area ========================================================================================================================================== -->
                                          ${tripInfo.route.from.text}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="10" valign="top"></td>
                                      </tr>
                                      <tr>
                                        <td valign="top">
                                          <img src="http://pickndrop.com.bd/public_profile/public_images/dropoff-6HGVpV3jlJwL23Q.png" height = "10" width="10" style="width:10;vertical-align:center;margin-right:10px;border:none;outline:none;text-decoration:none" class="CToWUd">
  <!-- To Area =========================================================================================================================================== -->
                                          ${tripInfo.route.to.text}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="40" valign="top"></td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top">
                                          <table border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                              <tr>
                                                <td colspan="2" bgcolor="#f3f3f3" style="padding:5px">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#353535"><strong>Trip Information</strong></font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#212121">
                                                    - Base Fare</font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- Base Fare ========================================================================================================================================= -->
                                                    Tk ${tripInfo.billing.baseFare}
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td width="60%">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- Trip Time ========================================================================================================================================== -->
                                                    - Trip Time (${tripInfo.billing.tripTime} min)
                                                  </font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- Trip Time Cost ===================================================================================================================================== -->
                                                    Tk ${tripInfo.billing.tripTimeBill}
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td width="60%">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    - Waiting Time (${tripInfo.billing.waitingTime} min)
                                                  </font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    Tk ${tripInfo.billing.waitingTimeBill}
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td width="60%">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    - Discount
                                                  </font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    ${tripInfo.billing.discount}%
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <td height="1" class="m_-8177750052882491647tronhr" style="background-color:#c6c6c6;font-size:0px;line-height:0px" bgcolor="#c6c6c6"><img src="https://ci6.googleusercontent.com/proxy/bjlmwp1lfIp2fevkoEO9P7LRLJDB3htzJ41P3i-rSE5ZwtA6fw0XQ__C7XIfeKMxLCjsuN8uGnjTZ77lPg_2Q3X-nIE1drc87j_Mf3u2_Iy1exK-tg=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/no_collapse.png" style="outline:none;text-decoration:none;display:block;" class="CToWUd">
                                              </td>
                                              <td height="1" class="m_-8177750052882491647tronhr" style="background-color:#c6c6c6;font-size:0px;line-height:0px" bgcolor="#c6c6c6"><img src="https://ci6.googleusercontent.com/proxy/bjlmwp1lfIp2fevkoEO9P7LRLJDB3htzJ41P3i-rSE5ZwtA6fw0XQ__C7XIfeKMxLCjsuN8uGnjTZ77lPg_2Q3X-nIE1drc87j_Mf3u2_Iy1exK-tg=s0-d-e1-ft#http://d1a3f4spazzrp4.cloudfront.net/receipt_v2/no_collapse.png" style="outline:none;text-decoration:none;display:block" class="CToWUd">
                                              </td>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td width="60%">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
                                                    - Subtotal
                                                  </font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    Tk ${tripInfo.billing.subTotalBill}
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td width="60%">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
                                                    - Rounding Down
                                                  </font>
                                                </td>
                                                <td align="right">
                                                  <font style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
  <!-- ======================================================================================================================================================= -->
                                                    (Tk ${tripInfo.billing.roundDown})
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td bgcolor="#f3f3f3" style="padding:5px">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#353535">
                                                    <strong>
                                                      TOTAL
                                                    </strong>
                                                  </font>
                                                </td>
                                                <td bgcolor="#f3f3f3" style="padding:5px" align="right">
                                                  <font style="font-family:Helvetica,sans-serif;font-size:12px;color:#353535">
                                                    <strong>
  <!-- ======================================================================================================================================================= -->
                                                      Tk ${tripInfo.billing.totalBill}
                                                    </strong>
                                                  </font>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td colspan="2" align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#212121">
                                                  If you have any query or suggestion about our service, don't hesitate to call us at +880 1788 99 33 88 (<span
                                                  class="aBn" data-term="goog_1260896153" tabindex="0"><span class="aQJ">8AM - 8PM</span></span>).
                                                  <br>
                                                  <br>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td width="25" align="left" valign="top" bgcolor="#ffffff" style="background:#ffffff">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td height="20" align="center" valign="top" style="height:20px!important;line-height:20px!important;font-size:20px!important;color:#ededed!important;height:20px;line-height:20px;font-size:20px;color:#ededed">.</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#1b2b3a" style="background:#1b2b3a">
          <table width="600" class="m_-6246123157711681189mobilewrapper" border="0" bgcolor="#1b2b3a" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;background:#1b2b3a">
            <tbody>
              <tr>
                <td align="center" valign="top" bgcolor="#1b2b3a" style="background:#1b2b3a">
                  <table width="600" class="m_-6246123157711681189mobilewrapper" border="0" bgcolor="#1b2b3a" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;background:#1b2b3a">
                    <tbody>
                      <tr>
                        <td align="center" valign="top" bgcolor="#1b2b3a" style="background:#1b2b3a">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" bgcolor="#1b2b3a">
                          <table width="180" height="37" class="m_-6246123157711681189width180Social" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;display:inline-block;width:180px">
                            <tbody>
                              <tr>
                                <td align="center" valign="top">
                                  <table width="180" height="37" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;width:180px">
                                    <tbody>
                                      <tr>
                                        <td colspan="7" width="180" height="7" align="center" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#1b2b3a;font-size:7px;line-height:7px;width:180px;height:7px">.</td>
                                      </tr>
                                      <tr>
                                        <td width="70" height="22" align="left" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#ffffff;font-size:14px;line-height:22px;height:22px;width:70px"><span style="background:#1b2b3a;color:#ffffff;font-size:14px;line-height:22px;height:22px;width:70px">Find&nbsp;us&nbsp;on:</span></td>
                                        <td align="center" width="10" height="22" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#1b2b3a;text-decoration:none;border:none;outline:none;width:10px;height:22px;font-size:22px;line-height:22px">.</td>
                                        <td align="center" width="22" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;text-decoration:none;border:none;outline:none;width:22px;height:22px">

  <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                          <a href="https://www.facebook.com/pickndropteam/" style="font-size:15px;font-family:Arial,Helvetica,sans-serif;color:#ffffff;text-decoration:none;border:none;outline:none;height:22px" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/facebook_icon.jpg"
                                            width="22" height="22" alt="" style="vertical-align:top;text-decoration:none;border:none;outline:none;width:22px;height:22x" class="CToWUd"></a>
                                        </td>
                                        <td align="center" width="10" height="22" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#1b2b3a;text-decoration:none;border:none;outline:none;width:10px;height:22px;font-size:22px;line-height:22px">.</td>
                                        <td align="center" width="26" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;text-decoration:none;border:none;outline:none;width:26px;height:22px">

  <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                          <a href="http://twitter.com/pickndropteam" style="font-size:15px;font-family:Arial,Helvetica,sans-serif;color:#ffffff;text-decoration:none;border:none;outline:none;height:22px" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/twitter_icon.jpg"
                                            width="26" height="22" alt="" style="vertical-align:top;text-decoration:none;border:none;outline:none;width:26px;height:22x" class="CToWUd"></a>
                                        </td>
                                        <td align="center" width="10" height="22" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#1b2b3a;text-decoration:none;border:none;outline:none;width:10px;height:22px;font-size:22px;line-height:22px">.</td>
                                        <td align="center" width="26" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;text-decoration:none;border:none;outline:none;width:26px;height:22px">

  <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                          <a href="http://pickndrop.com.bd" style="font-size:15px;font-family:Arial,Helvetica,sans-serif;color:#ffffff;text-decoration:none;border:none;outline:none;height:22px" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/google_plus_icon.jpg"
                                            width="26" height="22" alt="" style="vertical-align:top;text-decoration:none;border:none;outline:none;width:26px;height:22x" class="CToWUd"></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="7" width="180" height="8" align="center" valign="middle" bgcolor="#1b2b3a" style="background:#1b2b3a;color:#1b2b3a;font-size:8px;line-height:8px;width:180px;height:8px">.</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table width="162" height="5" class="m_-6246123157711681189width162Blank" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;display:inline-block;width:162px;height:5px">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" bgcolor="#1b2b3a" style="font-family:Arial,Helvetica,sans-serif;font-size:5px;color:#1b2b3a;text-align:center;background:#1b2b3a" class="m_-6246123157711681189width162BlankTD">Lorem Ipsum dolor sit amet, consectetur adpisicing elit, sed do eiusmod to te inciddidunt ut labore. Ut enim ad minim veniam.</td>
                              </tr>
                            </tbody>
                          </table>
                          <table width="240" class="m_-6246123157711681189width240" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;display:inline-block">
                            <tbody>
                              <tr>
                                <td align="center" valign="top">
                                  <table width="240" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="top">
  <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                          <a href="http://pickndrop.com.bd"
                                          style="border:none;outline:none;text-decoration:none" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/app_store_iconn.png"
                                            width="115" height="37" alt="App" style="border:none;outline:none;text-decoration:none;vertical-align:top" class="CToWUd"></a>
                                        </td>
                                        <td width="5" align="center" valign="top">&nbsp;</td>
                                        <td align="center" valign="top">
  <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                          <a href="http://pickndrop.com.bd"
                                          style="border:none;outline:none;text-decoration:none" target="_blank" data-saferedirecturl=""><img src="http://pickndrop.com.bd/public_profile/public_images/google_play_icon.png"
                                            width="115" height="37" alt="Google" style="border:none;outline:none;text-decoration:none;vertical-align:top" class="CToWUd"></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" bgcolor="#1b2b3a" style="background:#1b2b3a">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  `;
}

function getMailBodyInText(tripInfo) {
  return `From: ${tripInfo.route.from.text}
  | To: ${tripInfo.route.to.text}
  | Base price: ${tripInfo.billing.baseFare}
  | Total time: ${tripInfo.billing.tripTime}
  | Waiting time: ${tripInfo.billing.waitingTime}
  | Discount: ${tripInfo.billing.discount}
  | Subtotal bill: ${tripInfo.billing.subTotalBill}
  | Rounding Down: (${tripInfo.billing.roundDown})
  | Total Bill: ${tripInfo.billing.totalBill}`;
}

function sendReceipt(recipients, tripInfo, fn) {
  const mailOptions = {
    from: '"Pick N Drop" <receipt@pickndrop.com.bd>',
    to: recipients,
    subject: 'Re: "Pick N Drop" Receipts',
    text: getMailBodyInText(tripInfo),
    html: getReceiptBody(tripInfo)
  };

  receiptTransport.sendMail(mailOptions, (err, info) => {
    fn(err, info);
  });
}

function sendTestMail(recipients, fn) {
  const mailOptions = {
    from: '"Pick N Drop" <reciept@pickndrop.com.bd>',
    to: recipients,
    subject: 'Pick N Drop Receipts',
    text: 'this is a test text',
    html: 'this is a test html'
  };

  noreplyTransport.sendMail(mailOptions, (err, info) => {
    fn(err, info);
  });
}

export default {
  sendReceipt,
  sendTestMail,
};
