function password() {
  let pass = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#$@0123456789';

  for (let i = 0; i < 6; i += 1) {
    pass += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return pass;
}

function imageName() {
  function randomString() {
    return Math.floor((1 + Math.random()) * 0x1000000)
      .toString(16)
      .substring(1);
  }
  return randomString() + randomString() + randomString();
}

function verificationCode() {
  let generatedPassword = '';
  const possible = '0123456789';

  for (let i = 0; i < 6; i += 1) {
    generatedPassword += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return generatedPassword;
}


export default { password, imageName, verificationCode };
