import Promise from 'bluebird';
import GoogleMaps from 'googlemaps';
import config from '../../config/config';

const GMAP = new GoogleMaps({
  key: config.gmapApiKey,
  stagger_time: 1000,
  encode_polylines: false,
  secure: true
});

function getDistance(lat1, lon1, lat2, lon2) {
  const R = 6371; // Radius of the earth in km
  const dLat = (lat2 - lat1) * (Math.PI / 180);
  const dLon = (lon2 - lon1) * (Math.PI / 180);
  const a =
    (Math.sin(dLat / 2) * Math.sin(dLat / 2)) +
    (Math.cos(lat1 * (Math.PI / 180)) *
    Math.cos(lat2 * (Math.PI / 180)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2));
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return R * c; // Distance in km
}

function getDuration(from, to) {
  const params = {
    origins: from,
    destinations: to
  };
  return new Promise((resolve, reject) => {
    GMAP.distance(params, (err, distanceValue) => {
      if (err) reject('Error');
      else resolve(distanceValue);
    });
  });
}

function getTripImage(fromPosition, toPositon, tripPath) {
  return GMAP.staticMap({
    size: '525x200',
    maptype: 'roadmap',
    markers: [{
      location: fromPosition,
      // icon: 'http://50.63.165.217:5312/static/markers/start_point.png'
    }, {
      location: toPositon,
      // icon: 'http://50.63.165.217:5312/static/markers/end_point.png'
    }],
    path: [{
      color: 'black',
      weight: '2',
      geodesic: true,
      points: tripPath
    }]
  });
}

function getTripPath(_ttl) {
  if (_ttl.length > 50) {
    let _tbd = _ttl.length - 50;
    let newIndex = Math.round(_ttl.length / _tbd);
    return _ttl.filter((arr, index) => {
      if (index === newIndex) {
        _tbd -= 1;
        newIndex += Math.round((_ttl.length - newIndex) / _tbd);
        return false;
      }
      return true;
    });
  }
  return _ttl;
}

export default {
  getDistance,
  getDuration,
  getTripImage,
  getTripPath
};
