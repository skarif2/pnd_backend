import Promise from 'bluebird';
import request from 'request';
import config from '../../config/config';

const options = {
  user: config.bkash.username,
  pass: config.bkash.password,
  msisdn: config.bkash.merchant
};

function _send(transactionId, cb) {
  options.transactionId = transactionId;

  const requestOptions = {
    uri: 'https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg',
    method: 'POST',
    headers: { Accept: 'application/json' },
    json: options
  };
  request(requestOptions, (err, res, resBody) => {
    if (err) {
      return cb(err);
    }
    return cb(null, resBody);
  });
}

function checkTransaction(trxnId) {
  return new Promise((resolve, reject) => {
    _send(options, trxnId, (err, response) => {
      const transaction = response.transaction;
      if (err) reject('failed to connect bkash');
      else if (transaction.trxStatus === '0010' || transaction.trxStatus === '0011') reject('Your transaction is pending. Please try after some time.');
      else if (transaction.trxStatus === '0100') reject('Your transection has been reversed.');
      else if (transaction.trxStatus === '0111') reject('Your transection has been failed. Please sending money again.');
      else if (transaction.trxStatus !== '0000') reject('Invalid transaction ID. Please try using a valid transaction ID.');
      else resolve(transaction);
    });
  });
}

export default { checkTransaction };
