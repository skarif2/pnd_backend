import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Trip Schema
 */
const TripSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  driverId: {
    type: String,
    required: true
  },
  publicId: {
    type: String,
    required: true
  },
  vehicleType: {
    type: String,
    required: true
  },
  route: {
    from: {
      text: String,
      lat: String,
      lng: String
    },
    to: {
      text: String,
      lat: String,
      lng: String
    }
  },
  tripTiming: {
    accept: {
      type: Date,
      default: Date.now
    },
    arrive: {
      type: Date,
      default: Date.now
    },
    start: {
      type: Date,
      default: Date.now
    },
    end: {
      type: Date,
      default: Date.now
    }
  },
  tripState: {
    type: Number,
    default: 0
  },
  tripPath: [],
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
TripSchema.method({
});

/**
 * Statics
 */
TripSchema.statics = {
  /**
   * Get Trip
   * @param {ObjectId} id - The objectId of Trip.
   * @returns {Promise<Trip, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((trip) => {
        if (trip) {
          return trip;
        }
        const err = new APIError('No such trip exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get trip of user
   * @param {userId} userId - The userId of user.
   * @returns {Promise<User, APIError>}
   */
  getToCheckExists(userId) {
    return this.findOne({ 'user.userId': userId })
      .exec()
      .then((user) => {
        if (user) {
          const err = new APIError('You are already in a trip. Please restart Pick N Drop app.', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        }
        return 'No trip exists!';
      });
  }
};

/**
 * @typedef Trip
 */
export default mongoose.model('Trip', TripSchema);
