import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Warning Schema
 */
const WarningSchema = new mongoose.Schema({
  drivers: [],
  requestId: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
WarningSchema.method({
});

/**
 * Statics
 */
WarningSchema.statics = {
  /**
   * Get Warning
   * @param {ObjectId} id - The objectId of Request.
   * @returns {Promise<Warning, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((warning) => {
        if (warning) {
          return warning;
        }
        const err = new APIError('No such warning exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef Warning
 */
export default mongoose.model('Warning', WarningSchema);
