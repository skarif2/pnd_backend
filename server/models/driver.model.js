import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Driver Schema
 */
const DriverSchema = new mongoose.Schema({
  publicId: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true,
    unique: true,
    match: [/^[1-9][0-9]{12}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  image: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  vehicle: {
    vehicleType: {
      type: String,
      required: true
    },
    vehicleModel: {
      type: String,
      required: true
    },
    vehicleNumber: {
      type: String,
      required: true
    }
  },
  status: {
    isLoggedIn: {
      type: Boolean,
      default: false
    },
    isActive: {
      type: Boolean,
      default: false
    },
    isOnTrip: {
      type: Boolean,
      default: false
    },
    isPendingBill: {
      type: Boolean,
      default: false
    }
  },
  account: {
    balance: {
      type: Number,
      default: 0
    },
    paid: {
      type: Number,
      default: 0
    }
  },
  rating: {
    value: {
      type: Number,
      default: 5
    },
    count: {
      type: Number,
      default: 1
    }
  },
  location: {
    updateTime: {
      type: String,
      default: '1496218715',
    },
    lat: {
      type: String,
      default: '23.782090'
    },
    lng: {
      type: String,
      default: '90.425590'
    }
  },
  lastLocation: {
    lat: {
      type: String,
      default: '23.782090'
    },
    lng: {
      type: String,
      default: '90.425590'
    }
  },
  pushCode: {
    type: String,
    default: ''
  },
  warning: {
    total: {
      limit: {
        type: Number,
        default: 0
      },
      accept: {
        type: Number,
        default: 0
      },
      appKill: {
        type: Number,
        default: 0
      }
    },
    today: {
      date: {
        type: Date,
        default: Date.now
      },
      limit: {
        type: Number,
        default: 0
      },
      accept: {
        type: Number,
        default: 0
      },
      appKill: {
        type: Number,
        default: 0
      }
    }
  },
  tripCount: {
    total: {
      finished: {
        type: Number,
        default: 0
      },
      unfinished: {
        type: Number,
        default: 0
      }
    },
    today: {
      date: {
        type: Date,
        default: Date.now
      },
      finished: {
        type: Number,
        default: 0
      },
      unfinished: {
        type: Number,
        default: 0
      }
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
DriverSchema.method({
});

/**
 * Statics
 */
DriverSchema.statics = {
  /**
   * Get driver
   * @param {ObjectId} id - The objectId of driver.
   * @returns {Promise<driver, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((driver) => {
        if (driver) {
          return driver;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get driver
   * @param {publicId} publicId - The publicId of driver.
   * @returns {Promise<driver, APIError>}
   */
  getByPublicId(publicId) {
    return this.findOne({ publicId })
      .exec()
      .then((driver) => {
        if (driver) {
          return driver;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get single driver
   * @param {mobileNumber} mbNo - The mobileNumber of user.
   * @returns {Promise<driver, APIError>}
   */
  getToCheckExists(mbNo) {
    return this.findOne({ mobileNumber: mbNo })
      .exec()
      .then((driver) => {
        if (driver) {
          const err = new APIError('The mobile number is already in use.', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        }
        return 'No such user exists!';
      });
  },

  /**
   * Get single user
   * @param {mobileNumber} mbNo - The mobileNumber of user.
   * @returns {Promise<User, APIError>}
   */
  getByMobileNumber(mbNo) {
    return this.findOne({ mobileNumber: mbNo })
      .exec()
      .then((driver) => {
        if (driver) {
          return driver;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get single user
   * @param {vehicleType} vehicleType - The vehicleType of driver.
   * @returns {Promise<User, APIError>}
   */
  getAvailable(vehicleType) {
    return this.find({ 'vehicle.vehicleType': vehicleType, 'status.isActive': true })
      .exec()
      .then((drivers) => {
        if (drivers) {
          return drivers;
        }
        const err = new APIError('No drivers available!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<driver[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef driver
 */
export default mongoose.model('Driver', DriverSchema);
