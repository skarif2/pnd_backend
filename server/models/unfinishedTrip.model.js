import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * UnfinishedTrip Schema
 */
const UnfinishedTripSchema = new mongoose.Schema({
  tripId: {
    type: String,
    required: true
  },
  user: {
    userId: String,
    mobileNumber: String,
    image: String,
    name: String
  },
  driver: {
    driverId: String,
    publicId: String,
    mobileNumber: String,
    image: String,
    name: String
  },
  vehicleType: {
    type: String,
    required: true
  },
  route: {
    from: {
      text: String,
      lat: String,
      lng: String
    },
    to: {
      text: String,
      lat: String,
      lng: String
    }
  },
  tripTiming: {
    accept: {
      type: Date,
      default: Date.now
    },
    arrived: {
      type: Date,
      default: Date.now
    },
    start: {
      type: Date,
      default: Date.now
    },
    end: {
      type: Date,
      default: Date.now
    }
  },
  tripState: {
    type: Number,
    default: 0
  },
  cancelledBy: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UnfinishedTripSchema.method({
});

/**
 * Statics
 */
UnfinishedTripSchema.statics = {
  /**
   * Get UnfinishedTrip
   * @param {ObjectId} id - The objectId of UnfinishedTrip.
   * @returns {Promise<UnfinishedTrip, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((unfinishedTrip) => {
        if (unfinishedTrip) {
          return unfinishedTrip;
        }
        const err = new APIError('No such unfinishedTrip exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef UnfinishedTrip
 */
export default mongoose.model('UnfinishedTrip', UnfinishedTripSchema);
