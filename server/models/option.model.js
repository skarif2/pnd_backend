import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Option Schema
 */
const OptionSchema = new mongoose.Schema({
  bonus: {
    signUp: {
      text: {
        type: String,
        default: 'Sign Up Bonus'
      },
      value: {
        type: Number,
        default: 150
      }
    }
  },
  area: {
    bike: Number,
    car: Number
  },
  billing: {
    bike: {
      ttPerMin: Number,
      wtPerMin: Number,
      baseFare: Number
    },
    car: {
      ttPerMin: Number,
      wtPerMin: Number,
      baseFare: Number
    }
  },
  discount: {
    text: String,
    value: Number
  },
  serviceTime: {
    open: {
      type: Number,
      default: 1
    },
    close: {
      type: Number,
      default: 1439
    }
  },
  maxCancel: {
    type: Number,
    default: 10
  },
  maintenance: {
    isOn: {
      type: Boolean,
      default: false
    },
    endDate: {
      type: String,
      default: 'Tomorrow.'
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
OptionSchema.method({
});

/**
 * Statics
 */
OptionSchema.statics = {
  /**
   * Get Option
   * @param {ObjectId} id - The objectId of Option.
   * @returns {Promise<Option, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((option) => {
        if (option) {
          return option;
        }
        const err = new APIError('No such option exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef Option
 */
export default mongoose.model('options', OptionSchema);
