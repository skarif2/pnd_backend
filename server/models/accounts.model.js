import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Account Schema
 */
const AccountSchema = new mongoose.Schema({
  publicId: {
    type: String,
    required: true
  },
  driverId: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true,
    match: [/^[1-9][0-9]{12}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  name: {
    type: String,
    required: true
  },
  balanceDue: {
    type: Number,
    required: true
  },
  balancePaid: {
    type: Number,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
AccountSchema.method({
});

/**
 * Statics
 */
AccountSchema.statics = {
  /**
   * Get driver
   * @param {ObjectId} id - The objectId of driver.
   * @returns {Promise<driver, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((account) => {
        if (account) {
          return account;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<driver[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef account
 */
export default mongoose.model('Account', AccountSchema);
