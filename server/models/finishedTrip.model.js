import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * FinishedTrip Schema
 */
const FinishedTripSchema = new mongoose.Schema({
  tripId: {
    type: String,
    required: true
  },
  user: {
    userId: String,
    mobileNumber: String,
    image: String,
    name: String
  },
  driver: {
    driverId: String,
    publicId: String,
    mobileNumber: String,
    image: String,
    name: String
  },
  vehicleType: {
    type: String,
    required: true
  },
  route: {
    from: {
      text: String,
      lat: String,
      lng: String
    },
    to: {
      text: String,
      lat: String,
      lng: String
    }
  },
  tripTiming: {
    accept: {
      type: Date,
      default: Date.now
    },
    arrive: {
      type: Date,
      default: Date.now
    },
    start: {
      type: Date,
      default: Date.now
    },
    end: {
      type: Date,
      default: Date.now
    }
  },
  tripPath: [],
  billing: {
    totalBill: Number,
    subTotalBill: Number,
    roundDown: Number,
    baseFare: Number,
    ttPerMin: Number,
    wtPerMin: Number,
    tripTime: Number,
    tripTimeBill: Number,
    waitingTime: Number,
    waitingTimeBill: Number,
    discount: Number,
    isPaid: {
      type: Boolean,
      default: false
    }
  },
  snapshot: {
    type: String,
    required: true
  },
  rating: {
    user: {
      type: Number,
      default: 0
    },
    driver: {
      type: Number,
      default: 0
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
FinishedTripSchema.method({
});

/**
 * Statics
 */
FinishedTripSchema.statics = {
  /**
   * Get finishedTrip
   * @param {ObjectId} id - The objectId of FinishedTrip.
   * @returns {Promise<FinishedTrip, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((finishedTrip) => {
        if (finishedTrip) {
          return finishedTrip;
        }
        const err = new APIError('No such trip exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List history of user in descending order of 'createdAt' timestamp.
   * @param {string} userId - userId of user.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list(userId, { skip = 0, limit = 20 } = {}) {
    return this.find({ 'user.userId': userId })
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },
};

/**
 * @typedef FinishedTrip
 */
export default mongoose.model('FinishedTrip', FinishedTripSchema);
