import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true,
    unique: true,
    match: [/^[1-9][0-9]{12}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  image: {
    type: String,
    required: true
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  account: {
    balance: {
      type: Number,
      default: 0
    },
    spent: {
      type: Number,
      default: 0
    }
  },
  rating: {
    value: {
      type: Number,
      default: 5
    },
    count: {
      type: Number,
      default: 1
    }
  },
  verification: {
    code: String,
    verified: {
      type: Boolean,
      default: false
    }
  },
  location: {
    lat: {
      type: String,
      default: '23.782090'
    },
    lng: {
      type: String,
      default: '90.425590'
    }
  },
  pushCode: {
    type: String,
    default: ''
  },
  device: {
    deviceId: {
      type: String,
      default: ''
    },
    deviceOs: {
      type: String,
      default: ''
    }
  },
  tripCount: {
    finished: {
      type: Number,
      default: 0
    },
    unfinished: {
      type: Number,
      default: 0
    }
  },
  cancelToday: {
    count: {
      type: Number,
      default: 0
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserSchema.method({
});

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },

  /**
   * Get single user
   * @param {mobileNumber} mbNo - The mobileNumber of user.
   * @returns {Promise<User, APIError>}
   */
  getToCheckExists(mbNo) {
    return this.findOne({ mobileNumber: mbNo })
      .exec()
      .then((user) => {
        if (user) {
          const err = new APIError('The mobile number is already in use.', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        }
        return 'No such user exists!';
      });
  },

  /**
   * Get single user
   * @param {mobileNumber} mbNo - The mobileNumber of user.
   * @returns {Promise<User, APIError>}
   */
  getByMobileNumber(mbNo) {
    return this.findOne({ mobileNumber: mbNo })
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get single user
   * @param {mobileNumber} mbNo - The mobileNumber of user.
   * @param {code} code - User verification code.
   * @returns {Promise<User, APIError>}
   */
  getToVerify(mbNo, code) {
    return this.findOne({ mobileNumber: mbNo })
      .exec()
      .then((user) => {
        if (user) {
          if (user.verification.code === code) {
            return user;
          }
          const err = new APIError('Invalid verification code!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
};

/**
 * @typedef User
 */
export default mongoose.model('User', UserSchema);
