import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Request Schema
 */
const RequestSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  vehicleType: {
    type: String,
    required: true
  },
  route: {
    from: {
      text: String,
      lat: String,
      lng: String
    },
    to: {
      text: String,
      lat: String,
      lng: String
    }
  },
  drivers: [{
    publicId: String,
    pushCode: String
  }],
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
RequestSchema.method({
});

/**
 * Statics
 */
RequestSchema.statics = {
  /**
   * Get Request
   * @param {ObjectId} id - The objectId of Request.
   * @returns {Promise<Request, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((request) => {
        if (request) {
          return request;
        }
        const err = new APIError('No such request exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get trip of user
   * @param {userId} userId - The userId of user.
   * @returns {Promise<User, APIError>}
   */
  getToCheckExists(userId) {
    return this.findOne({ 'user.userId': userId })
      .exec()
      .then((user) => {
        if (user) {
          const err = new APIError('We are processing your previous request, please wait for a while.', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        }
        return 'No request found!';
      });
  }
};

/**
 * @typedef Request
 */
export default mongoose.model('Request', RequestSchema);
