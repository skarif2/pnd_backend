const strings = {
  fcmBody: 'You just received a notification from Pick N Drop',
  fcmBodyAccept: 'We have found you a driver',
  fcmBodyArrive: 'Your ride is at your pick-up location',
  fcmBodyStart: 'Have a safe trip',
  fcmBodyEnd: 'Thank you for riding with us',
  fcmBodyBusy: 'Sorry! the drivers are busy. Please try after sometime.',
  fcmBodyCancel: 'We are sorry! Your trip has been cancelled',
  fcmBodyWarning: 'You can not move without a trip',
  userCancel: 'Sorry! The trip has been cancelled by the user.',
};

export default strings;
