import Joi from 'joi';

export default {
/* User param validations ======================================================================= */

  // POST /api/users/signup
  createUser: {
    body: {
      name: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required(),
      email: Joi.string().required(),
      password: Joi.string().required()
    }
  },

  // POST /api/users/verify
  verifyUser: {
    body: {
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required(),
      code: Joi.string().required()
    }
  },

  // POST /api/auth/userLogin
  userLogin: {
    body: {
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required(),
      password: Joi.string().required(),
      pushCode: Joi.string().required(),
      deviceId: Joi.string().required(),
      deviceOs: Joi.string().required()
    }
  },

  // POST /api/auth/userLogin
  userLogout: {
    body: {
      userId: Joi.string().hex().required(),
      deviceId: Joi.string().required()
    }
  },

  // POST /api/users/history
  history: {
    body: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/users/rePassword
  rePassword: {
    body: {
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required()
    }
  },

  // POST /api/users/reVerify
  reVerifyUser: {
    body: {
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required()
    }
  },

  // POST /api/users/account
  userAccount: {
    body: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/users/update
  updateUser: {
    body: {
      userId: Joi.string().hex().required(),
      deviceId: Joi.string().required()
    }
  },

  // POST /api/users/detailedHistory
  detailedHistory: {
    body: {
      tripId: Joi.string().hex().required()
    }
  },

  // POST /api/users/detailedHistory
  availableDrivers: {
    body: {
      currentLat: Joi.string().required(),
      currentLng: Joi.string().required(),
      vehicleType: Joi.string().required()
    }
  },

  // POST /api/users/estimation
  estimation: {
    body: {
      from: Joi.string().required(),
      to: Joi.string().required(),
      vehicleType: Joi.string().required()
    }
  },

  // POST /api/users/request
  createRequest: {
    body: {
      userId: Joi.string().hex().required(),
      currentLat: Joi.string().required(),
      currentLng: Joi.string().required(),
      from: Joi.string().required(),
      to: Joi.string().required(),
      fromLat: Joi.string().required(),
      fromLng: Joi.string().required(),
      toLat: Joi.string().required(),
      toLng: Joi.string().required(),
      vehicleType: Joi.string().required()
    }
  },

  // DELETE /api/users/request
  deleteRequest: {
    body: {
      requestId: Joi.string().hex().required()
    }
  },

  // POST /api/users/state
  userState: {
    body: {
      userId: Joi.string().hex().required(),
      deviceId: Joi.string().required()
    }
  },

/* Driver param validations ===================================================================== */

  // POST /api/drivers/account
  createDriver: {
    body: {
      publicId: Joi.string().required(),
      name: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required(),
      password: Joi.string().required(),
      vehicleType: Joi.string().required(),
      vehicleModel: Joi.string().required(),
      vehicleNumber: Joi.string().required(),
    }
  },

  // PUT /api/drivers/account
  updateDriver: {
    body: {
      driverId: Joi.string().hex().required(),
      timestamp: Joi.string().required(),
      lat: Joi.string().required(),
      lng: Joi.string().required()
    }
  },

  // POST /api/drivers/pushCode
  pushCode: {
    body: {
      driverId: Joi.string().hex().required(),
      pushCode: Joi.string().required()
    }
  },

  // POST /api/auth/userLogin
  driverLogin: {
    body: {
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{12}$/).required(),
      password: Joi.string().required(),
      pushCode: Joi.string().required()
    }
  },

  // POST /api/auth/userLogin
  driverLogout: {
    body: {
      driverId: Joi.string().hex().required()
    }
  },

  // POST /api/drivers/active
  active: {
    body: {
      driverId: Joi.string().hex().required()
    }
  },

  // POST /api/drivers/inactive
  inactive: {
    body: {
      driverId: Joi.string().hex().required()
    }
  },

  // POST /api/drivers/trip
  createTrip: {
    body: {
      requestId: Joi.string().hex().required(),
      driverId: Joi.string().hex().required(),
      warningId: Joi.string().hex().required(),
      publicId: Joi.string().hex().required()
    }
  },

  // PUT /api/drivers/trip
  updateTrip: {
    body: {
      tripId: Joi.string().hex().required(),
      job: Joi.string().required()
    }
  },

  // POST /api/drivers/finishTrip
  createFinishTrip: {
    body: {
      tripId: Joi.string().hex().required()
    }
  },

  // PUT /api/drivers/finishTrip
  updateFinishTrip: {
    body: {
      finishTripId: Joi.string().hex().required(),
      method: Joi.string().required()
    }
  },

  // POST /api/users/state
  driverState: {
    body: {
      driverId: Joi.string().hex().required(),
    }
  },

/* Mixed param validations ===================================================================== */

  // DELETE /api/users/trip
  // DELETE /api/drivers/trip
  deleteTrip: {
    body: {
      tripId: Joi.string().hex().required()
    }
  },

  // PUT /api/users/rate
  // PUT /api/drivers/rate
  rate: {
    body: {
      finishTripId: Joi.string().hex().required(),
      rating: Joi.number().required()
    }
  },

/* Mapview param validations =================================================================== */

  // POST /api/mapview/firstLoad
  // POST /api/mapview/lifetime
  mapview: {
    body: {
      userId: Joi.string().hex().required(),
      publicId: Joi.number().required()
    }
  },
/* Panel param validations =================================================================== */

  // POST /api/panel/payment
  payment: {
    body: {
      driverId: Joi.string().hex().required(),
      balancePaid: Joi.number().required(),
      receivedBy: Joi.string().required()
    }
  },

  // POST /api/panel/bulkSms
  bulkSms: {
    body: {
      message: Joi.string().required(),
      receiver: Joi.string().required()
    }
  },
};
