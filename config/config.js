import Joi from 'joi';

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),
  PORT: Joi.number()
    .default(5258),
  MONGOOSE_DEBUG: Joi.boolean()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      then: Joi.boolean().default(true),
      otherwise: Joi.boolean().default(false)
    }),
  JWT_SECRET: Joi.string().required()
    .description('JWT Secret required to sign'),
  MONGO_HOST: Joi.string().required()
    .description('Mongo DB host url'),
  MONGO_PORT: Joi.number()
    .default(27017),
  MONGO_USER: Joi.string().required()
    .description('Mongo DB username'),
  MONGO_PASS: Joi.string().required()
    .description('Mongo DB password'),
  MONGO_DB: Joi.string().required()
    .description('Mongo DB database name'),
  GOOGLE_API_KEY: Joi.string().required()
    .description('Api key to use in google maps api'),
  PUSH_KEY: Joi.string().required()
    .description('Api key to use for firebase push messaging'),
  SSL_USERNAME: Joi.string().required()
    .description('SSL Wireless username'),
  SSL_PASSWORD: Joi.string().required()
    .description('SSL Wireless password'),
  SSL_SERVICEID: Joi.string().required()
    .description('SSL Wireless serviceId'),
  BKASH_USERNAME: Joi.string().required()
    .description('bKash merchant username'),
  BKASH_PASSWORD: Joi.string().required()
    .description('bKash merchant password'),
  BKASH_MERCHANT: Joi.string().required()
    .description('bKash merchant number'),
  RECEIPT_EMAIL: Joi.string().required()
    .description('Email address for receipt'),
  RECEIPT_PASS: Joi.string().required()
    .description('Password for receipt email'),
  NOREPLY_EMAIL: Joi.string().required()
    .description('Email address for no-reply'),
  NOREPLY_PASS: Joi.string().required()
    .description('Password for no-reply email'),
  INFO_EMAIL: Joi.string().required()
    .description('Email address for info'),
  INFO_PASS: Joi.string().required()
    .description('Password for info email'),
  DEPLOY_PATH: Joi.string().required()
    .description('Server path to deploy project'),
}).unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  mongo: {
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT,
    user: envVars.MONGO_USER,
    pass: envVars.MONGO_PASS,
    db: envVars.MONGO_DB
  },
  gmapApiKey: envVars.GOOGLE_API_KEY,
  fcmPushKey: envVars.PUSH_KEY,
  bkash: {
    username: envVars.BKASH_USERNAME,
    password: envVars.BKASH_PASSWORD,
    merchant: envVars.BKASH_MERCHANT
  },
  ssl: {
    username: envVars.SSL_USERNAME,
    password: envVars.SSL_PASSWORD,
    serviceId: envVars.SSL_SERVICEID
  },
  mail: {
    receiptEmail: envVars.RECEIPT_EMAIL,
    receiptPass: envVars.RECEIPT_PASS,
    noreplyEmail: envVars.NOREPLY_EMAIL,
    noreplyPass: envVars.NOREPLY_PASS,
    infoEmail: envVars.INFO_EMAIL,
    infoPass: envVars.INFO_PASS
  },
  deploy_path: envVars.DEPLOY_PATH
};

export default config;
